import matplotlib.pyplot as plt
import numpy as np
import math


def main():
    gamma = 1.4
    a4 = 374.14852665753
    P = [100e3, 30.313626521e3, 30.3136265212e3, 10e3]
    T = [348.4, 247.7277669779, 397.6440190159, 278.7]
    rho = [1.000092008, 0.4263647687, 0.2656204721, 0.125020472]
    u = [0, 293.268253371, 293.268253371, 0]
    x = [0, 0.275510884, 0.4866640264, 0.67596095202, 0.8324241191, 1]

    files = ["ST100second.txt", "ST200second.txt", "ST400second.txt", "ST800second.txt", "ST1600second.txt", "ST3200second.txt"]
    #files = ["ST100first.txt", "ST200first.txt", "ST400first.txt", "ST800first.txt", "ST1600first.txt", "ST3200first.txt"]

    xs = [None] * len(files)
    Ps = [None] * len(files)
    us = [None] * len(files)
    rhos = [None] * len(files)
    Ts = [None] * len(files)

    for i in range(len(files)):
        t1, x1, u1, P1, rho1, T1 = extractData(files[i])
        xs[i] = x1[-1]
        Ps[i] = P1[-1]
        us[i] = u1[-1]
        rhos[i] = rho1[-1]
        Ts[i] = T1[-1]

    absError, relError = getErrors(P, T, rho, u, x, xs, Ps, gamma, a4)
    dx, l2error = l2norm(xs, absError)
    xe, Pe, ue, Te, rhoe = expansionFan(1000, x, P, u, T, rho, gamma, a4)

    xan = [x[0]] + xe + [x[3]]*2 + [x[4]]*2 + [x[5]]
    Pan = [P[0]] + Pe + [P[1]]*2 + [P[2]] + [P[3]]*2
    Tan = [T[0]] + Te + [T[1]] + [T[2]]*2 + [T[3]]*2
    rhoan = [rho[0]] + rhoe + [rho[1]] + [rho[2]]*2 + [rho[3]]*2
    uan = [u[0]] + ue + [u[1]] + [u[2]]*2 + [u[3]]*2

    xplt = [xan] + xs
    Pplt = [Pan] +Ps
    Tplt = [Tan] +Ts
    rhoplt = [rhoan] +rhos
    uplt = [uan] +us

    plot_data(xplt, uplt, Pplt, rhoplt, Tplt)

    l2error1 = [1091.2147572259435, 672.16228668047358, 466.00451498490582, 322.85339382497921, 240.37510764162042, 177.7392981790712]
    l2error2 = [1122.6159141770099, 648.90954816275632, 543.19718378524692, 378.44763311168634, 281.02891887419901, 225.10711665230772]

    markerOptions = ["v", "s", "D", "h", "x", "o"]
    markerSize = [6,5,4,3,2.0,1.0]
    markerColour = ["b","g","r","c","y","k"]
    a = plt.figure(1)
    a_plots = []
    for j in range(len(relError)):
        a, = plt.plot(xs[j], relError[j], label=str(len(xs[j])) + " cells", marker=markerOptions[j], markersize=markerSize[j], color=markerColour[j], linewidth=False)
        a_plots.append(a)
    plt.xlabel("$x$ $(m)$")
    plt.ylabel("$Relative$ $error$ $in$ $pressure$ $(%)$")
    plt.title("Shock tube relative error in pressure distribution")
    plt.legend(handles=a_plots)
    plt.grid()
    plt.xlim(xmin=0, xmax=1)
    plt.ylim(ymin=-2, ymax=100)
    plt.show()


    print(dx, "\n", l2error)
    m1 = (math.log10(l2error1[-1])-math.log10(l2error1[0]))/(math.log10(dx[-1])-math.log10(dx[0]))
    m2 = (math.log10(l2error2[-1])-math.log10(l2error2[0]))/(math.log10(dx[-1])-math.log10(dx[0]))
    a = plt.figure(2)
    a, = plt.plot(dx, l2error1, marker="o", markersize=6,
                      linewidth=False, label="First order Euler\nlog-log grad =" + str(m1)[:5])
    b, = plt.plot(dx, l2error2, marker="v", markersize=6,
                      linewidth=False, label="Predictor corrector\nlog-log grad =" + str(m2)[:5])
    c, = plt.plot([dx[0],dx[-1]], [l2error1[0],l2error1[-1]], marker="o", markersize=0,
                  linewidth=1, color=a.get_color())
    d, = plt.plot([dx[0],dx[-1]], [l2error2[0],l2error2[-1]], marker="o", markersize=0,
                  linewidth=1, color=b.get_color())
    plt.xlabel("$\Delta x$ $(m)$")
    plt.ylabel("$L2norm$ $error$ $in$ $pressure$ $(Pa)$")
    plt.title("Mesh independent study on pressure")
    plt.legend(handles=[a,b])
    plt.grid()
    plt.show()


def extractData(file_name):
    data = open(file_name, "r")
    data = data.read()
    time = data[data.find("START TIME"):data.find("END TIME")].split(",")[1:-1]

    for i in range(len(time)):
        time[i] = float(time[i])

    pro_data = [None] * 5
    titles = ["POSITION", "VELOCITY", "PRESSURE", "DENSITY", "TEMPERATURE"]
    for i in range(5):
        temp_data = data[data.find("START " + titles[i]):data.find("END " + titles[i])].split("\n")[1:-1]
        for j in range(len(temp_data)):
            nlist = temp_data[j].split(",")[:-1]
            for k in range(len(nlist)):
                nlist[k] = float(nlist[k])
            temp_data[j] = nlist
        pro_data[i] = temp_data

    return time, pro_data[0], pro_data[1], pro_data[2], pro_data[3], pro_data[4]


def plot_data( x, u, P, rho, T):
    figures = [None] * 4
    labels = ["Velocity", "Pressure", "Density", "Temperature"]
    units = ["$(m/s)$", "$(Pa)$", "$(kg/m^3})$", "$(K)$"]
    data = [u, P, rho, T]
    markerOptions = ["", "v", "s", "D", "h", "x", "o"]
    markerSize = [7,6,5,4,3,2.0,1.0]
    markerColour = ["w","b","g","r","c","y","k"]
    for i in range(len(data)):
        a = plt.figure(i)
        a_plots = []
        for j in range(len(data[i])):
            if j == 0:
                a, = plt.plot(x[j], data[i][j], label="Analytical solution", marker=markerOptions[j],
                              markersize=0, linewidth=3.0,color="#8b8b8b")
            else:
                a, = plt.plot(x[j], data[i][j], label=str(len(x[j]))+" cells", marker=markerOptions[j],markersize=markerSize[j] , linewidth=False, color=markerColour[j])
            plt.xlim(xmin=0,xmax=1)
            a_plots.append(a)
        plt.xlabel("$x$ $(m)$")

        plt.ylabel(labels[i] + " " + units[i])
        plt.title("Shock Tube " + labels[i] + " distribution using first order Euler")
        plt.legend(handles=a_plots)
        plt.grid()
        figures[i] = a
    plt.show(figures)


def shockTubeValue(position, x, P, u, T, rho, gamma, a4):
    if x[0] < position <= x[1]:
        Pr = P[0]
        ur = u[0]
        Tr = T[0]
        rhor = rho[0]
    elif x[1] < position <= x[2]:
        ur = (position - x[1]) * (u[1] - u[0]) / (x[2] - x[1])
        Pr = P[0] * (1 - ur * (gamma - 1) / (2 * a4)) ** (2 * gamma / (gamma - 1))
        Tr = T[0] * (Pr / P[0]) ** ((gamma - 1) / gamma)
        rhor = rho[0] * (Pr / P[0]) ** (1 / gamma)
    elif x[2] < position <= x[3]:
        Pr = P[1]
        ur = u[1]
        Tr = T[1]
        rhor = rho[1]
    elif x[3] < position <= x[4]:
        Pr = P[2]
        ur = u[2]
        Tr = T[2]
        rhor = rho[2]
    elif x[4] < position <= x[5]:
        Pr = P[3]
        ur = u[3]
        Tr = T[3]
        rhor = rho[3]
    else:
        print(position)

    return position, Pr, ur, Tr, rhor


def l2norm(x, absError):
    dx = [None] * len(x)
    l2error = [None] * len(absError)
    for i in range(len(dx)):
        dx[i] = 1 / len(x[i])
        tempError = 0

        l2error[i] = (sum(np.multiply(absError[i], absError[i])) / len(absError[i]))**(1/2)
    return dx, l2error


def expansionFan(n, x, P, u, T, rho, gamma, a4):
    Pr = [None] * n
    xr = [None] * n
    ur = [None] * n
    Tr = [None] * n
    rhor = [None] * n

    for i in range(n):
        position = x[1] + i * (x[2] - x[1]) / n
        xr[i], Pr[i], ur[i], Tr[i], rhor[i] = shockTubeValue(position, x, P, u, T, rho, gamma, a4)
    return xr, Pr, ur, Tr, rhor


def getErrors(P, T, rho, u, x, xs, Ps, gamma, a4):
    absError = [None] * len(xs)
    relError = [None] * len(xs)

    for i in range(len(xs)):
        tempAbs = [None] * len(xs[i])
        tempRel = [None] * len(xs[i])
        for j in range(len(xs[i])):
            mP = Ps[i][j]
            mX = xs[i][j]
            position, Pr, ur, Tr, rhor = shockTubeValue(mX, x, P, u, T, rho, gamma, a4)
            tempAbs[j] = math.fabs(mP - Pr)
            tempRel[j] = 100*math.fabs(mP - Pr) / Pr
        absError[i] = tempAbs
        relError[i] = tempRel
    return absError, relError


main()

