import matplotlib.pyplot as plt


def main():
    P = [30.313626521e3, 30.3136265212e3, 78.041283793e3, 78.041283793e3]
    T = [397.6440190159, 397.6440190159, 533.7122328422, 533.7122328422]
    rho = [0.2656204721, 0.2656204721, 0.5094896327, 0.5094896327]
    u = [293.268253371, 293.268253371, 0, 0]
    x = [0.85, 0.9369011766, 0.9369011766, 1]

    #files = ["ST1600r1cfl0.48.txt", "ST1600r2cfl0.48.txt"]
    files = ["ST1600r1cfl0.3.txt", "ST1600r2cfl0.3.txt"]

    xs = [None] * len(files)
    Ps = [None] * len(files)
    us = [None] * len(files)
    rhos = [None] * len(files)
    Ts = [None] * len(files)
    for i in range(len(files)):
        t1, x1, u1, P1, rho1, T1 = extractData(files[i])
        xs[i] = x1[-1]
        Ps[i] = P1[-1]
        us[i] = u1[-1]
        rhos[i] = rho1[-1]
        Ts[i] = T1[-1]

    xplt = [x] + xs
    Pplt = [P] + Ps
    Tplt = [T] + Ts
    rhoplt = [rho] +rhos
    uplt = [u] +us

    plot_data(xplt, uplt, Pplt, rhoplt, Tplt)


def extractData(file_name):
    data = open(file_name, "r")
    data = data.read()
    time = data[data.find("START TIME"):data.find("END TIME")].split(",")[1:-1]

    for i in range(len(time)):
        time[i] = float(time[i])

    pro_data = [None] * 5
    titles = ["POSITION", "VELOCITY", "PRESSURE", "DENSITY", "TEMPERATURE"]
    for i in range(5):
        temp_data = data[data.find("START " + titles[i]):data.find("END " + titles[i])].split("\n")[1:-1]
        for j in range(len(temp_data)):
            nlist = temp_data[j].split(",")[:-1]
            for k in range(len(nlist)):
                nlist[k] = float(nlist[k])
            temp_data[j] = nlist
        pro_data[i] = temp_data

    return time, pro_data[0], pro_data[1], pro_data[2], pro_data[3], pro_data[4]


def plot_data( x, u, P, rho, T):
    figures = [None] * 4
    labels = ["Velocity", "Pressure", "Density", "Temperature"]
    units = ["$(m/s)$", "$(Pa)$", "$(kg/m^3)$", "$(K)$"]
    data = [u, P, rho, T]
    legend = ["First order Euler", "Predictor corrector"]
    markerOptions = ["", "v", "o", "D", "h", "x", "o"]
    markerSize = [7,3.0,1.0]
    markerColour = ["w","k","y","r","c","y","k"]
    for i in range(len(data)):
        a = plt.figure(i)
        a_plots = []
        for j in range(len(data[i])):
            if j == 0:
                a, = plt.plot(x[j], data[i][j], label="Analytical solution", marker=markerOptions[j],
                              markersize=0, linewidth=3.0,color="#8b8b8b")
            else:
                a, = plt.plot(x[j], data[i][j], label=legend[j-1], marker=markerOptions[j],markersize=markerSize[j] , linewidth=False, color=markerColour[j])
            plt.xlim(xmin=0.85, xmax=1)
            a_plots.append(a)
        plt.xlabel("$x$ $(m)$")

        plt.ylabel(labels[i] + " " + units[i])
        plt.title("Shock Tube reflection " + labels[i].lower() + " distribution, $CFL=0.48$")
        plt.legend(handles=a_plots)
        plt.grid()
        figures[i] = a
    plt.show(figures)


main()

