import L1d_solver


def main():

    file_name = "rerun" + ".txt"
    t_run = 0.6e-3
    dt_out = 1e-6
    cfl = 0.48
    order = 1

    viscousEffects = False
    heatTransfer = False
    losses = False
    alarm = False

    Tw = 273 + 20   # wall temperature in K
    epsilon = 5e-6  # wall roughness

    # [number of cells, start of slug position, end of slug position, pressure, temperature, C_v, gamma, R, Pr, T0, S1, mu0]
    slug_0 = [100, 0, 0.5, 1e7, 348.4, 717.5, 1.4, 287.0, 0.72, 273.1, 110.4, 16.77e-6]
    slug_1 = [100, 0.5, 1, 1e4, 278.7, 717.5, 1.4, 287.0, 0.72, 273.1, 110.4, 16.77e-6]
    slugs = [slug_0, slug_1]

    boundaries = ["wall", "gas-gas", "wall"]

    diameters = [[0, 1], [0.010, 0.010]]
    lCentres = []
    lCoeffs = []
    lLengths = []
    xBuffer = []

    diaphragms = [[""]]
    piston_variables = [[]]
    piston_constants = [[]]
    outflow_positions = [None, None]

    # DO NOT EDIT FROM HERE ONWARDS
    L1d_solver.solver(file_name, slugs, boundaries, piston_variables, piston_constants, diaphragms, outflow_positions,
                      t_run, dt_out, cfl, order, diameters, epsilon, Tw, viscousEffects, heatTransfer, losses, lCentres,
                      lCoeffs, lLengths, xBuffer, alarm)


main()
