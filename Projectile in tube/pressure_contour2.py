import tkinter as tk
from tkinter import filedialog
import matplotlib.pyplot as plt
import numpy as np
from numpy import ma
from matplotlib import colors, ticker, cm
from matplotlib.mlab import bivariate_normal

root = tk.Tk()
root.withdraw()

file_name = filedialog.askopenfilename()
#file_name = "DSTO13.txt"

def main():
    data = open(file_name, "r")
    data = data.read()
    time = data[data.find("START TIME")+11:data.find("END TIME")].split(",")[:-1]

    for i in range(len(time)):
        time[i] = float(time[i])

    pro_data = [None] * 6
    titles = ["POSITION", "VELOCITY", "PRESSURE", "DENSITY", "TEMPERATURE", "PISTON"]
    for i in range(6):
        temp_data = data[data.find("START " + titles[i]):data.find("END " + titles[i])].split("\n")[1:-1]
        for j in range(len(temp_data)):
            nlist = temp_data[j].split(",")[:-1]
            for k in range(len(nlist)):
                nlist[k] = float(nlist[k])
            temp_data[j] = nlist
        pro_data[i] = temp_data

    x, u, P, rho, T, xPiston = pro_data[0], pro_data[1], pro_data[2], pro_data[3], pro_data[4], pro_data[5]

    lagrangePlot(time, x, xPiston, P)


def select_data(time_step_start, time_step_end, steps, total_data):
    index_list = np.arange(time_step_start,time_step_end,steps).tolist()
    selected_data = [None] * len(index_list)
    iterations = 0
    for i in index_list:
        selected_data[iterations] = total_data[i]
        iterations += 1
    return selected_data


def process_data(time,x,u,P,rho,T,time_step_start,time_step_end,steps):
    data = [time, x, u, P, rho, T]
    new_data = [None]*6
    for i in range(6):
        new_data[i] = select_data(time_step_start, time_step_end, steps, data[i])
    print("processed")
    return new_data[0], new_data[1], new_data[2], new_data[3], new_data[4], new_data[5]


def find(myList, myValue):
    index = 0
    for i in range(len(myList)):
        if myValue < myList[i] and index == 0:
            index = i
    return index


def lagrangePlot(t, x, xPiston, P):
    nPistons = len(xPiston[0])
    X = x
    Y = [[None]*len(x[0])]*len(t)
    for i in range(len(t)):
        for j in range(len(x[0])):
            Y[i][j] = t[i]
    figa, ax = plt.subplots()
    cs = ax.contourf(X, Y, P, locator=ticker.LogLocator(), cmap=cm.coolwarm)
    #figa.colorbar(cs)
    #cbar = figa.colorbar(cs)

    plt.show()

    piston_plots = [None] * nPistons
    colours = ['grey', 'yellow', 'red']
    for i in range(nPistons):
        # print(xPiston[i])
        # print(t)
        # print("len p", len(xPiston[i]))
        # print("len t", len(t))
        p = plt.plot(xPiston[i], t, linewidth=3 ,label="Piston" + str(1 + i), color=colours[i])
        piston_plots[i] = p
    plt.xlabel("Cell center position (m)")
    plt.ylabel("Time (s)")
    plt.title("Lagrange diagram of gas gun with 20 m reservoir length")
    plt.grid()
    plt.show()


main()
