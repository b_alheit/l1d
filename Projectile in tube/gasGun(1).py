import math
import numpy as np
import L1d_solver
import csv
import time

'''
Define the area of the tube as a function on position (x) below.
'''


def main():
    '''
    Define the total running time (t_run), the time step that the solver will output (dt_out) and the cfl number below
    '''
    file_name = "gas gun example" + ".txt"
    t_run = 1
    dt_out = 0.01e-3
    cfl = 0.3
    order = 2
    viscousEffects = False
    heatTransfer = False
    losses = False
    alarm = False

    Tw = 273 + 20  # wall temperature in K
    epsilon = 5e-6  # wall roughness

    # [number of cells, start of slug position, end of slug position, pressure, temperature, C_v, gamma, R, Pr, T0, S1, mu0]

    slug_0 = [100, -6, -0.005, 1e5, 348.4, 717.5, 1.4, 287.0, 0.72, 273.1, 110.4, 16.77e-6]
    slug_1 = [5, 0.0, 6, 1, 278.7, 717.5, 1.4, 287.0, 0.72, 273.1, 110.4, 16.77e-6]
    slugs = [slug_0, slug_1]

    boundaries = ["wall", "piston", "outflow"]

    systemDiameters = [[-6.5, 8], [0.010, 0.010]]
    xBuffer = [3.001]
    lCentres = [3, 6.225]
    lCoeffs = [0.25, 0.25]
    lLengths = [0.5, 0.3]

    diaphragms = [[""]]
    pistonVar = [[0, [-0.005, 0, 0]]]
    pistonConst = [[0.005, 0.001, math.pi * 0.01 * 0.004, 0.0, 0.010]]
    outflow_positions = [None, 6.0]

    # DO NOT EDIT FROM HERE ONWARDS
    L1d_solver.solver(file_name, slugs, boundaries, pistonVar, pistonConst, diaphragms, outflow_positions,
                      t_run, dt_out, cfl, order, systemDiameters, epsilon, Tw, viscousEffects, heatTransfer, losses, lCentres,
                      lCoeffs, lLengths, xBuffer, alarm)


main()
