'''
Demonstrate use of a log color scale in contourf
'''

import matplotlib.pyplot as plt
import numpy as np
from numpy import ma
from matplotlib import colors, ticker, cm
from matplotlib.mlab import bivariate_normal
import tkinter as tk
from tkinter import filedialog

root = tk.Tk()
root.withdraw()

file_name = filedialog.askopenfilename()

data = open(file_name, "r")
data = data.read()
time = data[data.find("START TIME")+11:data.find("END TIME")].split(",")[:-1]

for i in range(len(time)):
    time[i] = float(time[i])

pro_data = [None] * 6
titles = ["POSITION", "VELOCITY", "PRESSURE", "DENSITY", "TEMPERATURE", "PISTON"]
for i in range(6):
    temp_data = data[data.find("START " + titles[i]):data.find("END " + titles[i])].split("\n")[1:-1]
    for j in range(len(temp_data)):
        nlist = temp_data[j].split(",")[:-1]
        for k in range(len(nlist)):
            nlist[k] = float(nlist[k])
        temp_data[j] = nlist
    pro_data[i] = temp_data

s, u, P, rho, T, xPiston = pro_data[0], pro_data[1], pro_data[2], pro_data[3], pro_data[4], pro_data[5]

N = 1000000000
x = np.linspace(-3.0, 3.0, N)
y = np.linspace(-2.0, 2.0, N)

X, Y = np.meshgrid(x, y)

#print(X)
print("****************************************************")
#print(Y)
print("****************************************************")
# A low hump with a spike coming out of the top right.
# Needs to have z/colour axis on a log scale so we see both hump and spike.
# linear scale only shows the spike.
z = (bivariate_normal(X, Y, 0.1, 0.2, 1.0, 1.0)
     + 0.1 * bivariate_normal(X, Y, 1.0, 1.0, 0.0, 0.0))

# Put in some negative values (lower left corner) to cause trouble with logs:
z[:5, :5] = -1
#print(z)
print("****************************************************")
# The following is not strictly essential, but it will eliminate
# a warning.  Comment it out to see the warning.
z = ma.masked_where(z <= 0, z)
#print(z)
print("****************************************************")
# Automatic selection of levels works; setting the
# log locator tells contourf to use a log scale:
Xs = s
#print(x)
#print(time)
Ys = [[None]*len(s[0])]*len(time)
for i in range(len(time)):
    for j in range(len(s[0])):
        Ys[i][j] = time[i]
fig, ax = plt.subplots()
cs = ax.contourf(X, Y, z, locator=ticker.LogLocator(), cmap=cm.coolwarm)
#cs = ax.contourf(Xs, Ys, P, locator=ticker.LogLocator(), cmap=cm.coolwarm)
# Alternatively, you can manually set the levels
# and the norm:
#lev_exp = np.arange(np.floor(np.log10(z.min())-1),
#                    np.ceil(np.log10(z.max())+1))
#levs = np.power(10, lev_exp)
#cs = P.contourf(X, Y, z, levs, norm=colors.LogNorm())

# The 'extend' kwarg does not work yet with a log scale.

cbar = fig.colorbar(cs)

plt.show()
