import matplotlib.pyplot as plt
import math


def main():
    gamma = 1.4
    Pr = 1e5
    Tr = 348.4
    R = 287.0
    d = 0.01
    mp = 0.001
    t = 0.047
    files = ["gasGun.txt", "gasGun6m.txt"]

    xs = [None] * len(files)
    Ps = [None] * len(files)
    us = [None] * len(files)
    rhos = [None] * len(files)
    Ts = [None] * len(files)
    ts = [None] * len(files)
    xp = [None] * len(files)
    up = [None] * len(files)
    Pback = [None] * len(files)

    for i in range(len(files)):
        ts[i], xs[i], us[i], Ps[i], rhos[i], Ts[i], xp[i], up[i] = extractData(files[i])
        index = find(ts[i], t)
        xp[i] = convertPist(xp[i])
        up[i] = convertPist(up[i])
        ts[i], xs[i], us[i], Ps[i], rhos[i], Ts[i], xp[i], up[i] = ts[i][:index], xs[i][:index], us[i][:index], Ps[i][:index], rhos[i][:index], Ts[i][:index], xp[i][:index], up[i][:index]
        Pback[i] = backingPressure(Ps[i], xp[i], xs[i])

    tan, xan, uan, Pan = xupDat(gamma, t, Pr, Tr, R, d, mp, 1000)

    tplt = [tan] + ts
    xplt = [xan] + xp
    uplt = [uan] + up
    Pplt = [Pan] + Pback


    plot_data(tplt, xplt, uplt, Pplt)

    uError = [None]*len(files)
    for i in range(len(files)):
        uError[i] = pistUError(gamma, ts[i], Pr, Tr, R, d, mp, up[i])

    markerOptions = ["v", "D", "D", "h", "x", "o"]
    markerSize = [6, 5, 4, 3, 2.0, 1.0]
    markerColour = ["#8b8b8b", "k", "r", "c", "y", "k"]
    a = plt.figure(1)
    a_plots = []
    legend = ["Non-reflective model", "Reflective model"]
    for j in range(len(uError)):
        a, = plt.plot(ts[j], uError[j], label=legend[j], marker=markerOptions[j],
                      markersize=2.5, color=markerColour[j], linewidth=1)
        a_plots.append(a)
    plt.xlabel("$Time$ $(s)$")
    plt.ylabel("$Relative$ $error$ $in$ $piston$ $velocity$ $(%)$")
    plt.title("Shock tube relative error in pressure distribution")
    plt.legend(handles=a_plots)
    plt.grid()

    plt.show()


def extractData(file_name):
    data = open(file_name, "r")
    data = data.read()
    time = data[data.find("START TIME"):data.find("END TIME")].split(",")[1:-1]

    for i in range(len(time)):
        time[i] = float(time[i])
    time = [0]+time
    print(time)
    pro_data = [None] * 7
    titles = ["POSITION", "VELOCITY", "PRESSURE", "DENSITY", "TEMPERATURE", "PISTON POSITIONS", "PISTON VELOCITIES"]
    for i in range(7):
        temp_data = data[data.find("START " + titles[i]):data.find("END " + titles[i])].split("\n")[1:-1]
        for j in range(len(temp_data)):
            nlist = temp_data[j].split(",")[:-1]
            for k in range(len(nlist)):
                    nlist[k] = float(nlist[k])
            temp_data[j] = nlist
        pro_data[i] = temp_data

    return time, pro_data[0], pro_data[1], pro_data[2], pro_data[3], pro_data[4], pro_data[5], pro_data[6]


def convertPist(pList):
    outList = [None]*len(pList)
    for i in range(len(pList)):
        outList[i] = pList[i][0]
    return outList


def plot_data(t,x,u,P):
    figures = [None] * 3
    labels = ["Position", "Velocity", "Driving pressure"]
    units = ["$(m)$", "$(m/s)$", "$(Pa)$"]
    data = [x, u, P]
    markerOptions = ["", "", "", "D", "h", "x", "o"]
    markerSize = [3,1,1,4,3,2.0,1.0]
    markerColour = ["#8b8b8b","k","#8b8207ff","r","c","y","k"]
    legend = ["Analytical solution","Non-reflective model", "Reflective model"]
    for i in range(len(data)):
        a = plt.figure(i)
        a_plots = []
        for j in range(len(data[i])):


            a, = plt.plot(t[j], data[i][j], label=legend[j], marker=markerOptions[j],
                              markersize=0, linewidth=markerSize[j],color=markerColour[j])

            a_plots.append(a)
        plt.xlabel("$t$ $(s)$")

        plt.ylabel(labels[i] + " " + units[i])
        plt.title("Projectile in tube " + labels[i].lower() + " vs time plot")
        plt.legend(handles=a_plots)
        plt.grid()
        figures[i] = a
    plt.show(figures)


def find(myList, myValue):
    index = 0
    for i in range(len(myList)):
        if myValue < myList[i] and index == 0:
            index = i
    return index


def backingPressure(P,xp,xs):
    Pback = [None] * len(xp)
    for i in range(len(xp)):
        for j in range(len(xs[i])-1):

            if xs[i][j+1] > xp[i] > xs[i][j]:
                Pback[i] = P[i][j]
    return Pback


def xupDat(gamma, t, Pr, Tr, R, d, mp, n):

    dt = t/(n-1)
    tout = [None] * (n)
    xout = [None] * (n)
    uout = [None] * (n)
    Pout = [None] * (n)

    for i in range(n):
        if i == 0:
            tout[i] = 0
        else:
            tout[i] = tout[i-1] + dt
        xout[i], uout[i], Pout[i] = xupAn(gamma, tout[i], Pr, Tr, R, d, mp)

    return tout, xout, uout, Pout


def xupAn(gamma, t, Pr, Tr, R, d, mp):
    a0 = (gamma * R * Tr) ** (1 / 2)
    A = math.pi * d ** 2 / 4
    udmax = Pr * A / mp
    umax = 2 * a0 / (gamma - 1)

    u = umax * (1 - (1 + ((gamma + 1) / (gamma - 1)) * (udmax / umax) * t) ** ((1 - gamma) / (gamma + 1)))
    x = umax * t - (umax**2/udmax)*((gamma-1)/2)*((1 + (gamma+1)/(gamma-1)*(udmax/umax)*t)**(2/(gamma+1))-1)
    P = Pr*(1-((gamma-1)/2)*(u/a0))**(2*gamma/(gamma-1))


    return x, u, P


def pistUError(gamma, t, Pr, Tr, R, d, mp, up):
    uError = [None]*len(up)
    for i in range(len(up)):

        x, uan, P = xupAn(gamma, t[i], Pr, Tr, R, d, mp)
        if uan == up[i]:
            uError[i] = 0
        else:
            uError[i] = 100*math.fabs((uan-up[i])/uan)
    return uError


main()

