"""
L1d set up file template
Coded by: Benjamin Alheit

The code contains a templates for a shock tube, projectile in tube and two-stage gas gun in a large doc-string at the
bottom of the code.
"""
import L1d_solver
import math


def main():

    file_name = "template" + ".txt"
    t_run = 0.6e-3
    dt_out = 1e-6
    cfl = 0.48
    order = 1

    viscousEffects = False
    heatTransfer = False
    losses = False
    alarm = False

    Tw = 273 + 20   # wall temperature in K
    epsilon = 5e-6  # wall roughness

    # [number of cells, start of slug position, end of slug position, pressure, temperature, C_v, gamma, R, Pr, T0, S1, mu0]
    slug_0 = [100, 0, 0.5, 1e7, 348.4, 717.5, 1.4, 287.0, 0.72, 273.1, 110.4, 16.77e-6]
    slug_1 = [100, 0.5, 1, 1e4, 278.7, 717.5, 1.4, 287.0, 0.72, 273.1, 110.4, 16.77e-6]
    slugs = [slug_0, slug_1]

    boundaries = ["wall", "gas-gas", "wall"]

    systemDiameters = [[0, 1], [0.010, 0.010]]
    lCentres = []
    lCoeffs = []
    lLengths = []
    xBuffer = []

    diaphragms = [[""]]
    piston_variables = [[]]
    piston_constants = [[]]
    outflow_positions = [None, None]

    # DO NOT EDIT FROM HERE ONWARDS
    L1d_solver.solver(file_name, slugs, boundaries, piston_variables, piston_constants, diaphragms, outflow_positions,
                      t_run, dt_out, cfl, order, systemDiameters, epsilon, Tw, viscousEffects, heatTransfer, losses, lCentres,
                      lCoeffs, lLengths, xBuffer, alarm)


main()


"""
***********************************************************************************************************************
******************************************************shock tube*******************************************************
***********************************************************************************************************************
    file_name = "shock tube" + ".txt"
    t_run = 0.6e-3
    dt_out = 1e-6
    cfl = 0.48
    order = 1

    viscousEffects = False
    heatTransfer = False
    losses = False
    alarm = False

    Tw = 273 + 20   # wall temperature in K
    epsilon = 5e-6  # wall roughness

    # [number of cells, start of slug position, end of slug position, pressure, temperature, C_v, gamma, R, Pr, T0, S1, mu0]
    slug_0 = [100, 0, 0.5, 1e7, 348.4, 717.5, 1.4, 287.0, 0.72, 273.1, 110.4, 16.77e-6]
    slug_1 = [100, 0.5, 1, 1e4, 278.7, 717.5, 1.4, 287.0, 0.72, 273.1, 110.4, 16.77e-6]
    slugs = [slug_0, slug_1]

    boundaries = ["wall", "gas-gas", "wall"]

    systemDiameters = [[0, 1], [0.010, 0.010]]
    lCentres = []
    lCoeffs = []
    lLengths = []
    xBuffer = []

    diaphragms = [[""]]
    piston_variables = [[]]
    piston_constants = [[]]
    outflow_positions = [None, None]
    
***********************************************************************************************************************
*************************************************projectile in tube****************************************************
***********************************************************************************************************************
    file_name = "projectile in tube" + ".txt"
    t_run = 1
    dt_out = 0.01e-3
    cfl = 0.3
    order = 1
    viscousEffects = False
    heatTransfer = False
    losses = False
    alarm = False

    Tw = 273 + 20  # wall temperature in K
    epsilon = 5e-6  # wall roughness

    # [number of cells, start of slug position, end of slug position, pressure, temperature, C_v, gamma, R, Pr, T0, S1, mu0]
    slug_0 = [100, -6, -0.005, 1e5, 348.4, 717.5, 1.4, 287.0, 0.72, 273.1, 110.4, 16.77e-6]
    slug_1 = [5, 0.0, 6, 1, 278.7, 717.5, 1.4, 287.0, 0.72, 273.1, 110.4, 16.77e-6]
    slugs = [slug_0, slug_1]

    boundaries = ["wall", "piston", "outflow"]

    systemDiameters = [[-6, 6], [0.010, 0.010]]
    xBuffer = []
    lCentres = []
    lCoeffs = []
    lLengths = []

    diaphragms = [[""]]
    pistonVar = [[0, [-0.005, 0, 0]]]
    pistonConst = [[0.005, 0.001, math.pi * 0.01 * 0.004, 0.0, 0.010]]
    outflow_positions = [None, 6.0]
    
***********************************************************************************************************************
*************************************************two-stage gas gun*****************************************************
***********************************************************************************************************************
    file_name = "two-stage gas gun" + ".txt"
    t_run = 0.1
    dt_out = 0.1e-3
    cfl = 0.3
    order = 2
    viscousEffects = True
    heatTransfer = True
    losses = True
    alarm = True

    Tw = 273 + 20  # wall temperature in K
    epsilon = 5e-6  # wall roughness


    # [number of cells, start of slug position, end of slug position, pressure, temperature, C_v, gamma, R, Pr, T0, s1, mu0]
    slug_a = [40, 0, 3, 75.0e6, 287.7, 717.5, 1.4, 286.69, 0.72, 273.1, 110.4, 1.677e-05]
    slug_b = [40, 3.225, 6.225, 173000.0, 287.7, 3120, 1.667, 2080, 0.67, 273.1, 79.4, 1.87e-05]
    slug_c = [40, 6.245, 9.245, 101000.0, 287.7, 717.5, 1.4, 286.69, 0.72, 273.1, 110.4, 1.677e-05]
    slugs = [slug_a, slug_b, slug_c]

    systemDiameters = [[-1, 3, 3.001, 6.225, 6.226, 10.225], [0.1428, 0.1428, 0.1, 0.1, 0.02, 0.02]]
    xBuffer = [6.225]
    lCentres = [3, 6.225]
    lCoeffs = [0.25, 0.25]
    lLengths = [0.5, 0.3]

    boundaries = ["wall", "piston", "diaphragm-piston", "outflow"]

    diaphragms = [["intact", 100000000.0], [""]]
    piston_variables = [[0, [3, 0, 0], [6.226, 0, 0]]]
    piston_constants = [[0.225, 15, math.pi * 0.01 * 0.01, 0, 0.1], [0.02, 0.005, math.pi * 0.02 * 0.01, 0, 0.02]]
    outflow_positions = [None, 9.225]

"""