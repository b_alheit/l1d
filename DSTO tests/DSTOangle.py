import math
import L1d_solver



def main():
    '''
    Define the total running time (t_run), the time step that the solver will output (dt_out) and the cfl number below
    '''
    file_name = "DSTO80-80-80" + ".txt"
    t_run = 0.1
    dt_out = 0.1e-3
    cfl = 0.3
    order = 2
    viscousEffects = True
    heatTransfer = True
    losses = True
    alarm = True
    angle = 85
    angle = angle*math.pi/180.0
    D = 0.1428
    d = 0.1
    dd = 0.02

    Tw = 273 + 20  # wall temperature in K
    epsilon = 5e-6  # wall roughness

    '''
    Define each gas slug and assemble them into a list of slugs. The array for each slug must take the form of:
    slug_a = [number of cells, start of slug position, end of slug position, pressure, temperature, C_v, gamma, R, 
                Pr, T0, S1, mu0]

    Example:
    slug_a = [40, 0, 0.4, 100*10**5, 287.7, 717.5, 1.4, 286.69]
    slug_b = [40, 0.42, 1, 100*10**3, 287.7, 717.5, 1.4, 286.69]
    slug_c = [40, 1.02, 1.5, 100*10**3, 287.7, 717.5, 1.4, 286.69]

    slugs =[slug_a, slug_b, slug_c]

    Useful values:
    Air = [number of cells, start of slug position, end of slug position, pressure, temperature, 717.5, 1.4, 286.69, 
            0.72, 273.1, 110.4, 16.77e-6]
    Helium = [number of cells, start of slug position, end of slug position, pressure, temperature, 3120, 1.667, 2080, 
            0.67, 273.1, 79.4, 18.7e-6]
    '''

    # [number of cells, start of slug position, end of slug position, pressure, temperature, C_v, gamma, R, Pr, T0, s1, mu0]
    slug_0 = [80, 0, 3, 3.4e6, 287.7, 717.5, 1.4, 286.69, 0.72, 273.1, 110.4, 1.677e-05]
    slug_1 = [80, 3.225, 6.225, 119e3, 287.7, 3120, 1.667, 2080, 0.67, 273.1, 79.4, 1.87e-05]
    slug_2 = [80, 6.245, 9.225, 101e3, 287.7, 717.5, 1.4, 286.69, 0.72, 273.1, 110.4, 1.677e-05]
    slugs = [slug_0, slug_1, slug_2]

    diameters = [[0, 3-(D-d)/(4*math.tan(angle)), 3+(D-d)/(4*math.tan(angle)), 6.225-(d-dd)/(4*math.tan(angle)), 6.225+(d-dd)/(4*math.tan(angle)), 10.225], [0.1428, 0.1428, 0.1, 0.1, 0.02, 0.02]]
    xBuffer = [6.2249]
    lCentres = [3, 6.225]
    lCoeffs = [0.25, 0.25]
    lLengths = [0.15, 0.15]

    '''
    Define the boundary conditions of the system in the order that they occur. Allowable boundary conditions:
        *"wall"
        *"gas-gas"
        *"diaphragm"
        *"piston"
        *"diaphragm-piston"
        *"outflow"

    Example:
        boundaries = ["wall", "piston", "diaphragm-piston", "outflow"]

    If the spelling is incorrect the code will not run.
    '''

    boundaries = ["wall", "piston", "diaphragm-piston", "outflow"]

    '''
    Define the diaphragm properties, the initial piston variables, the piston constants and the outflow positions.
    These take the form of:
        diaphragms = [["intact", burst pressure], [diaphragm 2],[""]] #it is necessary for '[""]' to be the last entry 
    in the array for the code to run.
        pistons_variables =[[time, [Left wall position, velocity, acceleration], [piston 2]...]]
        piston_constants = [[length, mass, Area of seal, coefficient of friction, diameter], [piston 2] ...]
        outflow_positions = [right outflow position, left outflow position]

    Example:
        diaphragms = [["intact",500*10**3],[""]]
        piston_variables = [[0, [0.2, 0, 0], [0.5, 0, 0]]]
        piston_constants = [[0.02, 0.001, math.pi * 0.01 * 0.004, 0.2,0.1], [0.02, 0.001, math.pi * 0.01 * 0.004, 0.2,0.02]]
        outflow_positions = [None, 1.5]
    '''
    diaphragms = [["intact", 70e6], [""]]
    piston_variables = [[0, [3, 0, 0], [6.225, 0, 0]]]
    piston_constants = [[0.225, 15, math.pi * 0.01 * 0.01, 0, 0.1], [0.02, 0.005, math.pi * 0.02 * 0.01, 0, 0.02]]
    outflow_positions = [None, 9.225]

    # DO NOT EDIT FROM HERE ONWARDS
    L1d_solver.solver(file_name, slugs, boundaries, piston_variables, piston_constants, diaphragms, outflow_positions,
                      t_run, dt_out, cfl, order, diameters, epsilon, Tw, viscousEffects, heatTransfer, losses, lCentres,
                      lCoeffs, lLengths, xBuffer, alarm)


main()
