import numpy as np
import math
import time
import winsound
'''
Lagrangian 1D CFD Code - rev8
UCT Mechanical engineering final year project 23
Coded by: Benjamin Alheit
'''


def solver(file_name, slugs, boundaries, pistonVar, pistonConst, diaphragms, outflow_positions, t_run, dt_out, cfl, order,
           systemDiameters, epsilon, Tw, viscousEffects, heatTransfer, losses, lCentres, lCoeffs, lLengths, xBuffer, alarm):

    t_start = time.time()               # Set time that the solving process began at

    # L1d solving functions: 1. Initiate slug data structures
    slugsConst, slugsVar = initiateDataStructures(slugs, systemDiameters)

    t = 0                               # Define the initial time in the simulation
    complete_old = 0                    # Define the percentage completion of the solver

    while t < t_run:

        try:
            # L1d solving functions: 2. Calculate Delta t
            dt = del_t(slugsVar, cfl)

            if t_run - t < dt:          # Ensures the simulation ends on the desired time
                dt = t_run - t

            for i in range(order):
                if t == 0:
                    # L1d solving functions: 3. Assign ghost cell values
                    slugsVar, diaphragms = assign_ghost_cells(boundaries, slugsVar, outflow_positions, diaphragms)

                # L1d solving functions: 4. Interpolate cell centre values to cell walls
                interpolated = interpolate(slugsVar)

                # L1d solving functions: 5. Determine intermediate pressure and velocity values
                intermediates = riemann_solver(slugsConst, interpolated, boundaries, pistonVar, diaphragms)

                # L1d solving functions: 6. Apply governing equations to cells
                slugsVar, pistonVar = governing_equations(slugsVar, intermediates, pistonVar, pistonConst, boundaries,
                                                          diaphragms, slugsConst, systemDiameters, epsilon, Tw,
                                                          viscousEffects, heatTransfer, losses, lCentres, lCoeffs,
                                                          lLengths)

                # L1d solving functions: 7. Apply time step
                slugsVar, pistonVar = time_step(dt, slugsVar, slugsConst, i, pistonVar, boundaries, systemDiameters,
                                                xBuffer, pistonConst)

                # L1d solving functions: 3. Assign ghost cell values
                slugsVar, diaphragms = assign_ghost_cells(boundaries, slugsVar, outflow_positions, diaphragms)

            t = slugsVar[-1][0]
            complete_new = round(100 * t / t_run)
            if complete_new - complete_old >= 1:
                print(str(complete_new) + "%")
                complete_old = complete_new
        except:
            break

    t_end = time.time()             # Set the time at which the solving process ended
    solve_time = t_end - t_start    # Calculate the time that the solving process took

    # Data processing functions: 1. Create file header
    fileHeader = createHeader(file_name, t_run, dt_out, cfl, order, viscousEffects, heatTransfer, losses, Tw, epsilon,
                              slugs, boundaries, systemDiameters, lCentres, lCoeffs, lLengths, xBuffer,
                              diaphragms, pistonVar, pistonConst, outflow_positions)

    # Data processing functions: 2. Remove ghost cells
    slugsVar = remove_ghost_cells(slugsVar)

    # Data processing functions: 3. Combine slugs into 'single slug'
    times, data = combine_slugs(slugsVar)

    # Data processing functions: 4. Find the maximum time step that was taken during solving
    max_dt = max_dt_finder(times)

    if max_dt > dt_out:  # Ensures that time resolution of the output text file is constant
        print("dt_out < max_dt \ndt_out =" + str(dt_out) + "max_dt =" + str(max_dt))
        dt_out = max_dt

    # Data processing functions: 5. Filter out data to ensure the desired output timestep
    times, data, piston_positions, pistonVelocities = filter_data(data, times, dt_out, pistonVar)

    # Data processing functions: 6. Convert arrays into strings to be written to a text file
    txt_data, time_str = convert_diff(data, times, piston_positions, pistonVelocities)

    # Data processing functions: 7. Write data to text file
    writeResults(file_name, fileHeader, solve_time, time_str, txt_data)

    if alarm:
        freq = 1000
        dur = int(10e3)
        winsound.Beep(freq, dur)


# *********************************************************************************************************************
# ************************************************L1d solving functions************************************************
# *********************************************************************************************************************


# 1. Initiate slug data structures
def initiateDataStructures(slugs, systemDiameters):
    """"
    This function constructs the slugsConst and slugsVar data structures. The slugsConst data structure contains all of
    the gas constants of each slug. The slugsVar data structure contains all of the variables for every cell in the
    simulation. Consult the project report for data structure trees.
    """
    slugsConst = []         # Array for slug constants
    slugsVar = [[0]]        # Data structure for slug variables
    for i in range(len(slugs)):
        slugsConst.append(slugs[i][5:])         # Insert constants from slug into slugsConst array

        s = np.zeros([slugs[i][0] + 4, 14])     # Initialise `s' data structure
        s[:, 3] = slugs[i][4] * slugs[i][5]     # Set the specific energy of each cell
        s[:, 8] = slugs[i][3]                   # Set the pressure of each cell
        s[:, 9] = slugs[i][4] * slugs[i][5]     # Set the specific internal energy of each cell
        s[:, 10] = (slugs[i][4] * slugs[i][6] * slugs[i][7]) ** (1 / 2)     # Sonic velocity of each cell
        s[:, 11] = slugs[i][3] / (slugs[i][4] * slugs[i][7])                # Density of each cell
        s[:, 12] = slugs[i][4]                                              # Temperature of each cell

        dx = (slugs[i][2] - slugs[i][1]) / slugs[i][0]                      # Length of each cell (equi-spaced mesh)

        for j in range(slugs[i][0]+4):
            s[j, 0] = slugs[i][1] + dx * (j - 3 / 2)    # Centre of each cell
            s[j, 1] = slugs[i][1] + dx * (j - 1)        # Right wall of each cell
            s[j, 13] = area(s[j, 1], systemDiameters)   # Area of right wall of each cell
            if j == 0:
                s[j, 7] = s[j, 11] * dx * (s[j, 13] + area(s[j, 0] - dx / 2, systemDiameters)) / 2  # Mass of 0th cell
            else:
                s[j, 7] = s[j, 11] * dx * (s[j, 13] + s[j - 1, 13]) / 2     # Mass of every other cell

        slugsVar[0].append(s)   # Store sub-data structure in slugsVar -> var0

    return slugsConst, slugsVar


# L1d solving functions: 2. Calculate Delta t
def del_t(slugsVar, cfl):
    """
    Calculate the time step based on the size of the cells, their sonic velocity and the cfl number
    """

    var_n = slugsVar[-1][1:]              # Set the data structure for the latest time step (excluding time value)

    for i in range(len(var_n)):           # Loop through all slugs
        for j in range(len(var_n[i])):    # Loop through the current slug's cells
            x_j = var_n[i][j][0]          # Position of cell centre
            x_jh = var_n[i][j][1]         # Position of the right cell wall
            u = var_n[i][j][2]            # Cell velocity
            a = var_n[i][j][10]           # Cell sonic velocity

            dx = 2 * math.fabs((x_jh - x_j))        # Calculating the length of the cell
            t_cell = cfl * dx / (math.fabs(u) + a)  # Calculating the allowable time step for cell j in slug i

            if i == 0 and j == 0:   # If the current cell is the first cell in the system
                t_min = t_cell      # Set the maximum allowable time step as the current cell time step

            if t_cell < t_min:  # If the current cell time step is less than the allowable time step for the system
                t_min = t_cell  # Set the allowable system time step to the current cell times step

    return t_min


# 3. Assign ghost cell values
def assign_ghost_cells(boundaries, slugsVar, outflow_positions, diaphragms):
    var_n = slugsVar[-1][1:]                # Set the data structure for the latest time step (excluding time value)
    diaphragm_count = 0                     # Initialise a value to determine the of 'diaphragm' boundary coditions that have been processed

    for i in range(len(boundaries)):
        # First boundary condition
        if i == 0:

            if boundaries[i] == "wall":
                var_n[i] = reflect(var_n[i], "left")

            if boundaries[i] == "outflow":
                var_n[i] = left_outflow(var_n[i], outflow_positions)

        # Last boundary condition
        elif i == len(boundaries) - 1:
            if boundaries[i] == "wall":
                var_n[i - 1] = reflect(var_n[i - 1], "right")

            if boundaries[i] == "outflow":
                var_n[i - 1] = right_outflow(var_n[i - 1], outflow_positions)

        # Internal boundary condition
        else:
            if boundaries[i] == "gas-gas":
                var_n = gas_gas(var_n, i)

            if boundaries[i] == "piston": # Ghost cell values don't matter for piston
                var_n[i - 1] = reflect(var_n[i - 1], "right")
                var_n[i] = reflect(var_n[i], "left")

            if boundaries[i] == "diaphragm-piston":
                if diaphragms[diaphragm_count][0] == "intact":  # Check to see in the diaphragm should burst
                    diaphragms[diaphragm_count] = check_burst(diaphragms[diaphragm_count], var_n[i - 1], var_n[i])

                var_n[i - 1] = reflect(var_n[i - 1], "right")  # After burst check apply reflective opperation anyway
                var_n[i] = reflect(var_n[i], "left")           # If the diaphragm has burst the intermediate values will be
                                                               # overwritten in the Riemann solver anyway

            if boundaries[i] == "diaphragm":
                if diaphragms[diaphragm_count][0] == "intact":  # Check to see if the diaphragm has busrt
                    diaphragms[diaphragm_count] = check_burst(diaphragms[diaphragm_count], var_n[i - 1], var_n[i])

                if diaphragms[diaphragm_count][0] == "intact":  # If its still 'intact'
                    var_n[i - 1] = reflect(var_n[i - 1], "right")
                    var_n[i] = reflect(var_n[i], "left")

                if diaphragms[diaphragm_count][0] == "burst":   # If it has 'burst'
                    var_n = gas_gas(var_n, i)

    slugsVar[-1][1:] = var_n    # Reassign values in data structure

    return slugsVar, diaphragms


# 3.1 Assign ghost cells based on reflective condition
def reflect(s_i, right_left):
        if right_left == "left":
            s_i[1][3:13] = s_i[2][3:13]              # setting scalar values as adjacent cell values
            s_i[1][0] = 2 * s_i[1][1] - s_i[2][0]  # setting position of cell centre
            s_i[1][2] = - s_i[2][2]                  # setting velocity of cell centre

            s_i[0][3:13] = s_i[3][3:13]              # setting scalar values as adjacent cell values
            s_i[0][13] = s_i[2][13]                  # setting area of right interface
            s_i[0][1] = 2 * s_i[1][0] - s_i[1][1]  # setting position of right interface
            s_i[0][0] = 2 * s_i[1][1] - s_i[3][0]  # setting position of cell centre
            s_i[0][2] = - s_i[3][2]                  # setting velocity of cell centre

        if right_left == "right":
            s_i[-2][3:13] = s_i[-3][3:13]                   # setting scalar values as adjacent cell values
            s_i[-2][0] = 2 * s_i[-3][1] - s_i[-3][0]      # setting position of cell centre
            s_i[-2][1] = 3 * s_i[-3][1] - 2 * s_i[-3][0]
            s_i[-2][2] = - s_i[-3][2]                       # setting velocity of cell centre

            s_i[-1][3:13] = s_i[-4][3:13]                   # setting scalar values as adjacent cell values
            s_i[-1][13] = s_i[-3][13]                       # setting area of right interface
            s_i[-1][1] = 2 * s_i[-3][1] - s_i[-5][1]      # setting position of right interface
            s_i[-1][0] = 2 * s_i[-3][1] - s_i[-4][0]      # setting position of cell centre
            s_i[-1][2] = - s_i[-4][2]                       # setting velocity of cell centre
        return s_i


# 3.2 Assign ghost cells based on gas-gas interface
def gas_gas(var_n, i):
        var_n[i - 1][-1] = var_n[i][3]
        var_n[i - 1][-2] = var_n[i][2]
        var_n[i][0] = var_n[i - 1][-4]
        var_n[i][1] = var_n[i - 1][-3]
        return var_n


# 3.3 Assign ghost cells based on right outflow condition
def right_outflow(s_i, outflow_positions):

        exit_cell = 0  # Initialise index of cell just on the outside of the exit
        for i in range(len(s_i)):

            if s_i[i][0] > outflow_positions[1]: # If the position of the current cell is outside of the exit
                break
            exit_cell += 1
        if exit_cell > len(s_i) - 2:
            exit_cell = len(s_i) - 2

        L_cell = (s_i[exit_cell][1] - s_i[exit_cell][0]) * 2    # Length of cell just inside the tube
        iterations = 0
        for i in range(exit_cell, len(s_i)):
            s_i[i][2:] = s_i[exit_cell - 1][2:]     # set all scalar values to be the same
            s_i[i][0] = s_i[exit_cell - 1][0] + L_cell * (iterations + 1)   # Determine the cell centre position
            s_i[i][1] = s_i[exit_cell - 1][1] + L_cell * (iterations + 1)   # Determine the right cell wall position
            iterations += 1

        return s_i


# 3.4 Assign ghost cells based on left outflow condition
def left_outflow(s_i, outflow_positions):
    exit_cell = 0
    for i in range(len(s_i)):
        if s_i[i][0] > outflow_positions[0]:
            break
        exit_cell += 1
    if exit_cell > len(s_i) - 2:
        exit_cell = len(s_i) - 2
    L_cell = (s_i[exit_cell][1] - s_i[exit_cell][0]) * 2
    iterations = 0
    for i in range(0, exit_cell):
        s_i[i][2:] = s_i[exit_cell][2:]
        s_i[i][0] = s_i[exit_cell][0] - L_cell * (exit_cell - iterations)
        s_i[i][1] = s_i[exit_cell][1] - L_cell * (exit_cell - iterations)
        iterations += 1
    return s_i


# 3.5 Check to see if a diaphragm has burst
def check_burst(diaphragm, left_slug, right_slug):
        P_L = left_slug[-3][8]      # Pressure on left of diaphragm
        P_R = right_slug[2][8]      # Pressure on right of diaphragm
        P_burst = diaphragm[1]      # Burst pressure of diaphragm
        if math.fabs(P_L - P_R) >= P_burst:
            diaphragm[0] = "burst"
        return diaphragm


# 4. Interpolate cell centre values to cell walls
def interpolate(slugsVar):
    var_n = slugsVar[-1][1:]            # Set the data structure for the latest time step
    interpolated = [0] * len(var_n)     # Create list to store interpolated values for system

    for i in range(len(var_n)):         # Loop through all slugs
        interpolated[i] = np.zeros([len(var_n[i]) - 3, 8])  # Create list to store interpolated values for internal cell walls

        for j in range(len(interpolated[i])):   # Loop through all cells in current slug
            x_jn1 = var_n[i][j][0]              # Cell centre position of far left cell
            x_j = var_n[i][j + 1][0]            # Cell centre position of cell left of wall
            x_jh = var_n[i][j + 1][1]           # Position of current wall
            x_j1 = var_n[i][j + 2][0]           # Cell centre position of cell right of wall
            x_j2 = var_n[i][j + 3][0]           # Cell centre position of far right cell

            places = [2, 10, 8, 11]             # Indicies for u, a, P and rho within data structure
            for k in places:
                phi_jn1 = var_n[i][j][k]        # Scalar value of far left cell
                phi_j = var_n[i][j + 1][k]      # Scalar value of immediate left cell
                phi_j1 = var_n[i][j + 2][k]     # Scalar value of immediate right cell
                phi_j2 = var_n[i][j + 3][k]     # Scalar value of far right cell

                # Calculate gradients
                Del_n_j = (phi_j - phi_jn1) / (x_j - x_jn1)
                Del_p_j = (phi_j1 - phi_j) / (x_j1 - x_j)
                Del_n_j1 = Del_p_j
                Del_p_j1 = (phi_j2 - phi_j1) / (x_j2 - x_j1)

                # Apply minmod interpolation
                phi_L = phi_j + (x_jh - x_j) * minmod(Del_n_j, Del_p_j)
                phi_R = phi_j1 + (x_jh - x_j1) * minmod(Del_n_j1, Del_p_j1)

                # Store in interpolated data structure
                interpolated[i][j][2 * places.index(k)] = phi_L
                interpolated[i][j][2 * places.index(k) + 1] = phi_R

    return interpolated


# 4.1 MinMod flux limiter
def minmod(p, n):
    """
    Flux limiting function
    """
    if p > 0 and n > 0 or p < 0 and n < 0:  # If p and n have the same sign
        if math.fabs(p) > math.fabs(n):  # If the magnitude of p is bigger than the magnitude of n
            x = n  # Return n
        else:
            x = p  # Return p
    else:
        x = 0  # Return 0
    return x


# 5. Determine intermediate pressure and velocity values
def riemann_solver(slugsConst, interpolated, boundaries, pistonVar, diaphragms):
    intermediates = [0] * len(interpolated)     # Initialising an array to store intermediate values
    piston_count = 0                            # Number of pistons processed
    diaphragm_count = 0                         # Number of diaphragms processed
    pVar_n = pistonVar[-1][1:]                  # Extracting current piston subtree
    for i in range(len(intermediates)):

        gamma = slugsConst[i][1]

        intermediates[i] = np.zeros([len(interpolated[i]), 2])
        for j in range(len(intermediates[i])):
            # Extract interpolated values from data structure
            u_L = interpolated[i][j][0]
            u_R = interpolated[i][j][1]
            a_L = interpolated[i][j][2]
            a_R = interpolated[i][j][3]
            P_L = interpolated[i][j][4]
            P_R = interpolated[i][j][5]
            rho_L = interpolated[i][j][6]
            rho_R = interpolated[i][j][7]

            # Calculate  Riemann invariants
            U_L = u_L + 2 * a_L / (gamma - 1)
            U_R = u_R - 2 * a_R / (gamma - 1)
            Z = (a_R / a_L) * (P_L / P_R) ** ((gamma - 1) / (2 * gamma))

            # Apply first stage Riemann slover equations
            P_s = P_L * (((gamma - 1) * (U_L - U_R)) / (2 * a_L * (1 + Z))) ** (2 * gamma / (gamma - 1))
            u_s = (U_L * Z + U_R) / (1 + Z)

            # Apply strong shock relations (if required)
            if P_s > 10 * P_R and P_s > 10 * P_L:
                P_s = rho_L * ((gamma + 1) / 2) * ((u_L - u_R) * (rho_R ** (1 / 2)) / (rho_R ** (1 / 2) + rho_L ** (1 / 2))) ** 2
                u_s = (u_L * (rho_L ** (1 / 2)) + u_R * (rho_R ** (1 / 2))) / (rho_L ** (1 / 2) + rho_R ** (1 / 2))

            if P_L * 10 > P_s > P_R * 10 or P_R * 10 > P_s > P_L * 10:
                for k in range(3):
                    if P_s <= 10 * P_L:
                        uLs = U_L - (2 * a_L / (gamma - 1)) * (P_s / P_L) ** ((gamma - 1) / (2 * gamma))
                        duls_dps = - (a_L / (gamma * P_L)) * (P_s / P_L) ** (-(gamma + 1) / (2 * gamma))
                    else:
                        uLs = u_L - (2 * P_s / (rho_L * (gamma + 1))) ** (1 / 2)
                        duls_dps = - (1 / (rho_L * (gamma + 1))) * (2 * P_s / (rho_L * (gamma + 1))) ** (-1 / 2)

                    if P_s <= 10 * P_R:
                        uRs = U_R + (2 * a_R / (gamma - 1)) * (P_s / P_R) ** ((gamma - 1) / (2 * gamma))
                        durs_dps = (a_R / (gamma * P_R)) * (P_s / P_R) ** (-(gamma + 1) / (2 * gamma))
                    else:
                        uRs = u_R + (2 * P_s / (rho_R * (gamma + 1))) ** (1 / 2)
                        durs_dps = (1 / (rho_R * (gamma + 1))) * (2 * P_s / (rho_R * (gamma + 1))) ** (-1 / 2)

                    Fn = uLs - uRs
                    dFn_dPs = (duls_dps - durs_dps) * P_s + (uLs - uRs)

                    P_s += - Fn / dFn_dPs

            # Apply isentropic relations if the cell wall is adjacent to a piston
            if (boundaries[i] == "piston" or boundaries[i] == "diaphragm-piston" and diaphragms[diaphragm_count][
                0] == "burst") and j == 0:
                u_s = pVar_n[piston_count][1]
                P_s = ((u_s - U_R) * ((gamma - 1) / (2 * gamma ** (1 / 2))) * (rho_R / (P_R ** (1 / gamma))) ** 0.5) ** (2 * gamma / (gamma - 1))
                piston_count += 1
                if boundaries[i] == "diaphragm-piston":
                    diaphragm_count += 1

            elif (boundaries[i + 1] == "piston" or (boundaries[i + 1] == "diaphragm-piston" and
                    diaphragms[diaphragm_count][0] == "burst")) and j == (len(intermediates[i]) - 1):
                u_s = pVar_n[piston_count][1]
                P_s = ((U_L - u_s) * ((gamma - 1) / (2 * gamma ** (1 / 2))) * (rho_L / (P_L ** (1 / gamma))) ** 0.5) ** (2 * gamma / (gamma - 1))

            elif boundaries[i] == "diaphragm-piston" and diaphragms[diaphragm_count][0] == "intact" and j == 0:
                piston_count += 1
                diaphragm_count += 1

            elif boundaries[i] == "diaphragm" and j == 0:
                diaphragm_count += 1


            # Store values in data structure
            intermediates[i][j][0] = P_s
            intermediates[i][j][1] = u_s

    return intermediates


# 6. Apply governing equations to cells
def governing_equations(slugsVar, intermediates, pistonVar, pistonConst, boundaries, diaphragms, slugsConst,
                        systemDiameters, epsilon, Tw, viscousEffects, heatTransfer, losses, lCentres, lCoeffs,
                        lLengths):
    s_n = slugsVar[-1][1:]   # Extracting latest slug sub data structure

    for i in range(len(s_n)):
        sC_i = slugsConst[i]
        for j in range(len(s_n[i]) - 4):
            u_jhn1 = intermediates[i][j][1]     # Intermediate velocity of left cell wall
            u_jh = intermediates[i][j + 1][1]   # Intermediate velocity of right cell wall
            P_jhn1 = intermediates[i][j][0]     # Intermediate pressure of right cell wall
            P_jh = intermediates[i][j + 1][0]   # Intermediate pressure of left cell wall

            x_jh = s_n[i][j + 2][1]             # Position of right wall
            x_jhn1 = s_n[i][j + 1][1]           # Position of left wall
            A_jh = s_n[i][j + 2][13]            # Area of right wall
            A_jhn1 = s_n[i][j + 1][13]          # Area of left wall

            # Cell scalar values
            m = s_n[i][j + 2][7]
            P = s_n[i][j + 2][8]
            u = s_n[i][j + 2][2]
            T = s_n[i][j + 2][12]
            a = s_n[i][j + 2][10]
            rho = s_n[i][j + 2][11]

            # Determine 3-D effects
            if viscousEffects:
                # 6.1 Determine forces in cells due to viscous shear
                Fwall = Friction_forces(x_jh, x_jhn1, sC_i, u, T, a, systemDiameters, rho, Tw, epsilon)
            else:
                Fwall = 0

            if heatTransfer:
                # 6.2 Determine heat loss in cells to the environment
                q = heat_transfer(x_jh, x_jhn1, sC_i, u, T, a, systemDiameters, rho, Tw, epsilon)
            else:
                q = 0

            if losses:
                aBar = (A_jh + A_jhn1) / 2
                # 6.3 Determine losses due to changes in flow area
                Floss = fLoss(lCentres, lCoeffs, lLengths, x_jh, x_jhn1, rho, u, aBar)
            else:
                Floss = 0

            if j == 0:
                s_n[i][1][4] = intermediates[i][0][1]   # Setting velocity of left wall of first non-ghost cell

            # Applying governing equations
            dxh_dt = u_jh
            du_dt = (P_jhn1 * A_jhn1 - P_jh * A_jh + P * (A_jh - A_jhn1) + Fwall + Floss) / m
            dE_dt = (P_jhn1 * A_jhn1 * u_jhn1 - P_jh * A_jh * u_jh + q) / m

            # Inserting values into slug sub-data structure
            s_n[i][j + 2][4] = dxh_dt
            s_n[i][j + 2][5] = du_dt
            s_n[i][j + 2][6] = dE_dt

    # Inserting sub data structure into parent data structure
    slugsVar[-1][1:] = s_n

    # Determine piston accelerations
    piston_count = 0        # Number of pistons processed
    diaphragm_count = 0     # Number of diaphragms processed
    p_n = pistonVar[-1][1:]     # Extracting the latest piston sub data structure

    for i in range(len(boundaries)):
        if boundaries[i] == "piston" or (boundaries[i] == "diaphragm-piston" and diaphragms[diaphragm_count][0] == "burst"):
            # Extract piston constants
            m_p = pistonConst[piston_count][1]
            A_seal = pistonConst[piston_count][2]
            mu = pistonConst[piston_count][3]

            # Extract required variables
            P_L = intermediates[i - 1][-1][0]    # Pressure of the left of the piston
            P_R = intermediates[i][0][0]         # Pressure of the right of the piston
            v_p = p_n[piston_count][1]
            d_p = pistonConst[piston_count][4]

            a_p = math.pi * d_p ** 2 / 4         # Area of piston

            F_f_max = mu * A_seal * math.fabs(P_R-P_L)  # Maximum possible friction of piston
            F_drive = a_p * (P_L - P_R)                 # Driving force of piston

            if math.fabs(v_p) >= 10 ** -6 or math.fabs(F_drive) >= F_f_max:
                F_f = np.sign(v_p) * F_f_max
            else:
                F_f = F_drive

            a = (F_drive - F_f) / m_p   # Acceleration of piston
            p_n[piston_count][2] = a    # Insert acceleration into data structure

        if boundaries[i] == "diaphragm-piston" and diaphragms[diaphragm_count][0] == "intact":
            p_n[piston_count][2] = 0    # Zero acceleration if diaphragm is intact

        if boundaries[i] == "piston":
            piston_count += 1

        if boundaries[i] == "diaphragm-piston":
            piston_count += 1
            diaphragm_count += 1

        if boundaries[i] == "diaphragm":
            diaphragm_count += 1

    # Insert sub piston trees into parent tree
    pistonVar[-1][1:] = p_n

    return slugsVar, pistonVar


# 6.1 Determine forces in cells due to viscous shear
def Friction_forces(x_jh, x_jhn1, sC_i, u, T, a, system_diameters, rho, Tw, epsilon):
    # Extract gas constants
    gamma = sC_i[1]
    Pr = sC_i[3]
    T0 = sC_i[4]
    S1 = sC_i[5]
    mu0 = sC_i[6]

    # Flow calculations
    D_bar = (diameter(x_jh, system_diameters) + diameter(x_jhn1, system_diameters)) / 2
    M = u / a
    mu = mu0 * ((T / T0) ** (3 / 2)) * ((T0 + S1) / (T + S1))
    Omega = Pr ** (1 / 2)
    Delta = 1 + (gamma - 1) * Omega * (M ** 2) / 2
    # 6.1.1 Determine the reynolds number
    Re = Reynolds_number(T, Tw, Delta, rho, mu, D_bar, u)

    if Re > 2000:
        Omega = Pr ** (1 / 3)
        Delta = 1 + (gamma - 1) * Omega * (M ** 2) / 2
        Re = Reynolds_number(T, Tw, Delta, rho, mu, D_bar, u)

    # 6.1.2 Determine the friction factor
    f = friction_factor(Delta, Re, epsilon, D_bar)

    # Determine shear stress and Fwall
    tau0 = -rho * f * u * math.fabs(u) / 8
    Fwall = tau0 * math.pi * D_bar * (x_jh - x_jhn1)

    return Fwall


# 6.1.1 Determine the reynolds number
def Reynolds_number(T, Tw, Delta, rho, mu, D_bar, u):
    # Gas  calculations
    Taw = Delta * T
    Ts = T + 0.5 * (Tw - T) + 0.22 * (Taw - T)
    mus = mu * T / Ts
    rhos = rho * T / Ts
    Re = rhos * D_bar * math.fabs(u) / mus
    return Re


# 6.1.2 Determine the friction factor
def friction_factor(Delta, Re, epsilon, D_bar):
    if Re == 0:
        f = 0

    if 2000 > Re > 0:
        f = 64 / (Delta * Re)

    if 4000 >= Re >= 2000:
        f = (0.032 / Delta) * ((Re / 2000) ** 0.3187)

    if Re > 4000:
        f = (1 / Delta) * (1.14 - 2 * math.log10(21.25 * Re ** (-0.9) + (epsilon / D_bar))) ** (-2)
    return f


# 6.2 Determine heat loss in cells to the environment
def heat_transfer(x_jh, x_jhn1, sC_i, u, T, a, system_diameters, rho, Tw, epsilon):
    # Extract gas constants
    Cv = sC_i[0]
    gamma = sC_i[1]
    Pr = sC_i[3]
    T0 = sC_i[4]
    S1 = sC_i[5]
    mu0 = sC_i[6]

    #Flow calculations
    D_bar = (diameter(x_jh, system_diameters) + diameter(x_jhn1, system_diameters)) / 2
    M = u / a
    mu = mu0 * ((T / T0) ** (3 / 2)) * ((T0 + S1) / (T + S1))
    Omega = Pr ** (1 / 2)
    Delta = 1 + (gamma - 1) * Omega * (M ** 2) / 2

    # 6.1.1 Determine the reynolds number
    Re = Reynolds_number(T, Tw, Delta, rho, mu, D_bar, u)
    if Re > 2000:
        Omega = Pr ** (1 / 3)
        Delta = 1 + (gamma - 1) * Omega * (M ** 2) / 2
        Re = Reynolds_number(T, Tw, Delta, rho, mu, D_bar, u)

    # 6.1.2 Determine the friction factor
    f = friction_factor(Delta, Re, epsilon, D_bar)

    # Calculate q
    St = (Pr ** (-2 / 3)) * f / 8
    Cp = Cv * gamma
    h = rho * Cp * math.fabs(u) * St
    Taw = Delta * T
    q = h * math.pi * D_bar * (x_jh - x_jhn1) * (Tw - Taw)
    return q


# 6.3 Determine losses due to changes in flow area
def fLoss(lCentres, lCoeffs, lLengths, x_jh, x_jhn1, rho, u, aBar):

    nLosses = len(lCentres)     # Number of losses in the system
    Floss = 0                   # Current loss on current cell

    for i in range(nLosses):

        kL = lCoeffs[i]                     # Extract loss coefficient
        lLoss = lLengths[i]                 # Extract the length of the loss
        xLEnd = lCentres[i] + lLoss / 2     # Determine the position where the loss ends
        xLStart = lCentres[i] - lLoss / 2   # Determine the position where the loss starts

        # Determine the length of the cell that is in the loss area
        if xLStart <= x_jh <= xLEnd and xLStart <= x_jhn1 <= xLEnd:
            dx = x_jh - x_jhn1
        elif xLStart <= x_jh <= xLEnd:
            dx = x_jh - xLStart
        elif xLStart <= x_jhn1 <= xLEnd:
            dx = xLEnd - x_jhn1
        else:
            dx = 0

        dPLoss = - kL * 0.5 * rho * u * math.fabs(u)    # Determine the pressure drop due to the loss
        Floss += dPLoss * aBar * dx / lLoss             # Add the current Floss to the sum of Flosses on the cell
    return Floss


# 7. Apply time step
def time_step(dt, slugsVar, slugsConst, iteration, pistonVar, boundaries, systemDiameters, xBuffer, pistonConst):
    # First iteration => First order Euler time step
    if iteration == 0:
        var_n = slugsVar[-1]                  # Extracting latest var sub data structure
        var_n1 = [0] * len(slugsVar[-1])      # Initiating new var sub data structure
        var_n1[0] = slugsVar[-1][0] + dt      # Setting time of new slug sub data structure

        for i in range(1, len(var_n)):        # Loop through slugs

            var_n1[i] = np.zeros([len(var_n[i]), 14])   # Initiate new sub sub data structure

            # Extract constants
            c_v = slugsConst[i - 1][0]
            gamma =slugsConst[i - 1][1]

            # Loop through non-ghost cells
            for j in range(2, len(var_n1[i]) - 2):

                m_0 = var_n[i][j][7]          # Mass of cell

                x_h0n1 = var_n[i][j - 1][1]   # Position of left cell wall before time step
                x_h0 = var_n[i][j][1]         # Position of right cell wall before time step
                u_0 = var_n[i][j][2]          # Velocity of cell centre before time step
                E_0 = var_n[i][j][3]          # Specific energy of cell before time step

                dxh_n1_dt_0 = var_n[i][j - 1][4]  # Rate of change of position of left cell wall
                dxh_dt_0 = var_n[i][j][4]         # Rate of change of position of right cell wall
                du_dt_0 = var_n[i][j][5]          # Rate of change of velocity of cell center
                dE_dt_0 = var_n[i][j][6]          # Rate of change of specific energy of cell



                # Invoke first order Euler time step
                x_h1n1 = x_h0n1 + dt * dxh_n1_dt_0      # Position of left cell wall after time step
                x_h1 = x_h0 + dt * dxh_dt_0             # Position of right cell wall after time step
                u_1 = u_0 + dt * du_dt_0                # Velocity of cell centre after time step
                E_1 = E_0 + dt * dE_dt_0                # Specific energy of cell after time step

                # 7.1 Apply equations of state
                x_1, m_1, A_h1, A_h1n1, rho_1, e_1, P_1, T_1, a_1 = equationsOfState(x_h1, x_h1n1, m_0, systemDiameters, E_1, u_1, gamma, c_v)

                if j == 2:  # If the current cell is the first non-ghost cell set its left wall position and area values
                    var_n1[i][j - 1][1] = x_h1n1
                    var_n1[i][j - 1][13] = A_h1n1

                # Insert the new values for each cell variable in the sub data structure
                var_n1[i][j][0] = x_1
                var_n1[i][j][1] = x_h1
                var_n1[i][j][2] = u_1
                var_n1[i][j][3] = E_1
                var_n1[i][j][7] = m_1
                var_n1[i][j][8] = P_1
                var_n1[i][j][9] = e_1
                var_n1[i][j][10] = a_1
                var_n1[i][j][11] = rho_1
                var_n1[i][j][12] = T_1
                var_n1[i][j][13] = A_h1

        # If there is a piston/projectile in the system, the time step is applied to it as well
        if "piston" in boundaries or "diaphragm-piston" in boundaries:
            pVar_n = pistonVar[-1]          # Extracting the latest piston sub data structure
            pVar_n1 = [0] * len(pVar_n)     # Initiating a new piston sub data structure
            pVar_n1[0] = pVar_n[0] + dt     # Setting the new time value

            # Loop through all pistons
            for i in range(1, len(pVar_n)):
                pVar_n1[i] = [None] * 3         # Initialising a new piston sub data structure
                x_L0 = pVar_n[i][0]             # The position of the left side of the piston before time step
                v_0 = pVar_n[i][1]              # The velocity of the piston before time step
                a_0 = pVar_n[i][2]              # The acceleration of the piston before time step
                pL = pistonConst[i - 1][0]      # The length of the piston
                x_R0 = x_L0 + pL                # The position of the front of the piston before time step

                x_L1 = x_L0 + v_0 * dt          # The position of the left side of the piston after time step
                v_1 = v_0 + a_0 * dt            # The velocity of the piston after time step
                x_R1 = x_L1 + pL                # The position of the right side of the piston after time step

                # Check to see if the piston has moved past a buffer, and adjust it and the gas variables if it has
                for j in range(len(xBuffer)):
                    # If the left side of the piston has crossed a buffer
                    if x_L0 >= xBuffer[j] and x_L1 < xBuffer[j]:
                        x_L1 = xBuffer[j]   # Set the position of the left side of the piston to the position of the buffer
                        x_R1 = x_L1 + pL    # Recalculate the position of the right side of the piston
                        v_1 = 0              # Set the velocity of the piston to zero

                        # General function
                        pos = pistonSlugPosition(boundaries, i)

                        # 7.2 Adjust cells and pistons based on buffer
                        var_n1 = bufferAdjust(var_n1, pos, x_L1, slugsConst, systemDiameters, x_R1)

                    # If the right side of the piston has crossed a buffer (simlarly to above)
                    if x_R0 <= xBuffer[j] and x_R1 > xBuffer[j]:
                        x_L1 = xBuffer[j] - pL
                        x_R1 = x_L1 + pL
                        v_1 = 0

                        # General function: Determine the index of the slug on the left side of the piston
                        pos = pistonSlugPosition(boundaries, i)
                        # 7.2 Adjust cells and pistons based on buffer
                        var_n1 = bufferAdjust(var_n1, pos, x_L1, slugsConst, systemDiameters, x_R1)

                # Insert new piston values into sub data structures
                pVar_n1[i][0] = x_L1
                pVar_n1[i][1] = v_1

            # Append new sub data structure to parent data structure
            pistonVar.append(pVar_n1)

        # Append new sub data structure to parent data structure
        slugsVar.append(var_n1)

    # Second iteration => Predictor corrector Euler time step
    if iteration == 1:

        var_n = slugsVar[-1]          # Extract the latest var sub data structure
        var_nn1 = slugsVar[-2]        # Extract the slug sub data structure before

        # Loop through all slugs
        for i in range(1, len(var_n)):
            # Extracting gas constants
            c_v = slugsConst[i - 1][0]
            gamma = slugsConst[i - 1][1]

            for j in range(2, len(var_n[i]) - 2):

                m_0 = var_nn1[i][j][7]              # Mass of cell

                x_h0n1 = var_nn1[i][j - 1][1]       # Position of left cell wall before time step
                x_h0 = var_nn1[i][j][1]             # Position of right cell wall before time step
                u_0 = var_nn1[i][j][2]              # Velocity of cell centre before time step
                E_0 = var_nn1[i][j][3]              # Specific energy of cell before time step

                dxh_n1_dt_0 = var_nn1[i][j - 1][4]  # Rate of change of position of left cell wall before time step
                dxh_dt_0 = var_nn1[i][j][4]         # Rate of change of position of right cell wall before time step
                du_dt_0 = var_nn1[i][j][5]          # Rate of change of velocity of cell center before time step
                dE_dt_0 = var_nn1[i][j][6]          # Rate of change of specific energy of cell before time step

                dxh_n1_dt_1 = var_n[i][j - 1][4]    # Rate of change of position of left cell wall after time step
                dxh_dt_1 = var_n[i][j][4]           # Rate of change of position of right cell wall after time step
                du_dt_1 = var_n[i][j][5]            # Rate of change of velocity of cell center after time step
                dE_dt_1 = var_n[i][j][6]            # Rate of change of specific energy of cell center after time step

                # Invoke predictor corrector time step
                x_h1n1 = x_h0n1 + dt * (dxh_n1_dt_0 + dxh_n1_dt_1) / 2  # Position of the left cell wall after time step
                x_h1 = x_h0 + dt * (dxh_dt_0 + dxh_dt_1) / 2    # Position of the right cell wall after time step
                u_1 = u_0 + dt * (du_dt_0 + du_dt_1) / 2        # Velocity of the left cell wall after time step
                E_1 = E_0 + dt * (dE_dt_0 + dE_dt_1) / 2        # Specific energy of the left cell wall after time step

                # 7.1 Apply equations of state
                x_1, m_1, A_h1, A_h1n1, rho_1, e_1, P_1, T_1, a_1 = equationsOfState(x_h1, x_h1n1, m_0,systemDiameters, E_1, u_1, gamma, c_v)

                if j == 2:  # If the current cell is the first non-ghost cell set its left wall position and area values
                    var_n[i][j - 1][1] = x_h1n1
                    var_n[i][j - 1][13] = A_h1n1

                # Insert the new values for each cell variable in the sub data structure
                var_n[i][j][0] = x_1
                var_n[i][j][1] = x_h1
                var_n[i][j][2] = u_1
                var_n[i][j][3] = E_1
                var_n[i][j][7] = m_1
                var_n[i][j][8] = P_1
                var_n[i][j][9] = e_1
                var_n[i][j][10] = a_1
                var_n[i][j][11] = rho_1
                var_n[i][j][12] = T_1
                var_n[i][j][13] = A_h1


        if "piston" in boundaries or "diaphragm-piston" in boundaries:

            pVar_n = pistonVar[-1]      # Extract the latest piston var data structure
            pVar_nn1 = pistonVar[-2]    # Extract the second latest piston var data structure

            # Loop through all pistons
            for i in range(1, len(pVar_n)):

                x_L0 = pVar_nn1[i][0]       # The position of the left side of the piston before time step
                v_0 = pVar_nn1[i][1]        # The velocity of the piston before time step
                a_0 = pVar_nn1[i][2]        # The acceleration of the piston before time step
                pL = pistonConst[i - 1][0]  # The length of the piston
                x_R0 = x_L0 + pL            # The position of the front of the piston before time step

                v_1 = pVar_n[i][1]          # Approximate piston velocity after time step
                a_1 = pVar_n[i][2]          # Approximate piston acceleration after time step

                x_L1 = x_L0 + (v_0 + v_1) * dt / 2  # Position of the left side of the piston after time step
                x_R1 = x_L1 + pL                    # Position of the right side of the piston after time step
                v_1 = v_0 + (a_0 + a_1) * dt / 2    # Velocity of the piston after time step

                # Check is the piston crossed any buffers
                for j in range(len(xBuffer)):

                    # If the left side of the piston crossed a buffer
                    if x_L0 >= xBuffer[j] and x_L1 < xBuffer[j]:
                        x_L1 = xBuffer[j]   # Set the position of the left side of the piston to the position of the buffer
                        x_R1 = x_L1 + pL    # Recalculate the position of the right side of the piston
                        v_1 = 0             # Set the velocity of the piston to 0

                        # General function: Determine the index of the slug on the left side of the piston
                        pos = pistonSlugPosition(boundaries, i)

                        # 7.2 Adjust cells and pistons based on buffer
                        var_n = bufferAdjust(var_n, pos, x_L1, slugsConst, systemDiameters, x_R1)

                    # If the right side of the piston crossed a buffer (similarly to above)
                    elif x_R0 <= xBuffer[j] and x_R1 > xBuffer[j]:
                        x_L1 = xBuffer[j] - pL
                        x_R1 = x_L1 + pL
                        v_1 = 0

                        # General function: Determine the index of the slug on the left side of the piston
                        pos = pistonSlugPosition(boundaries, i)

                        # 7.2 Adjust cells and pistons based on buffer
                        var_n = bufferAdjust(var_n, pos, x_L1, slugsConst, systemDiameters, x_R1)

                # Overwrite the values in the piston sub data structure
                pVar_n[i][0] = x_L1
                pVar_n[i][1] = v_1

            # Overwrite the values in the piston parent data structure
            pistonVar[-1] = pVar_n

        # Overwrite the values in the slugsVar parent data structure
        slugsVar[-1] = var_n
    return slugsVar, pistonVar


# 7.1 Apply equations of state
def equationsOfState(x_h1, x_h1n1, m_0, system_diameters, E_1, u_1, gamma, c_v):

    # Calculate flow state variables based on the positions of the cell walls and velocity and specific energy of the cell
    x_1 = (x_h1 + x_h1n1) / 2
    m_1 = m_0
    dx = (x_h1 - x_h1n1)
    A_h1n1 = area(x_h1n1, system_diameters)
    A_h1 = area(x_h1, system_diameters)
    A_bar = (A_h1 + A_h1n1) / 2
    rho_1 = m_1 / (A_bar * dx)
    e_1 = E_1 - 0.5 * (u_1 ** 2)
    P_1 = rho_1 * (gamma - 1) * e_1
    T_1 = e_1 / c_v
    a_1 = (gamma * (gamma - 1) * e_1) ** 0.5

    return x_1, m_1, A_h1, A_h1n1, rho_1, e_1, P_1, T_1, a_1


# 7.2 Adjust cells and pistons based on buffer
def bufferAdjust(var_n, pos, x_L1, slugsConst, systemDiameters, x_R1):

    cell_L = var_n[pos][-3]         # Extracting the cell to the left of the piston
    x_h1L = x_L1                    # Position of the right wall of the left cell
    x_h1n1L = var_n[pos][-4][1]     # Position of the left wall of the left cell
    E_L = cell_L[3]                 # Specific energy of the left cell
    u_L = cell_L[2]                 # Velocity of the left cell
    m_L = cell_L[7]                 # Mass of the left cell

    # Constants of the left cell. [pos - 1] because of the time value in the var_n data structure
    gamma_L = slugsConst[pos - 1][1]
    Cv_L = slugsConst[pos - 1][0]

    # Determine the new values of the left cell
    x_ln, m_ln, A_hln, A_hnln, rho_ln, e_ln, P_ln, T_ln, a_ln = equationsOfState(x_h1L, x_h1n1L, m_L, systemDiameters, E_L, u_L, gamma_L, Cv_L)

    # Overwrite the old values in the var_n data structure
    cell_L[0] = x_ln
    cell_L[1] = x_h1L
    cell_L[8] = P_ln
    cell_L[9] = e_ln
    cell_L[10] = a_ln
    cell_L[11] = rho_ln
    cell_L[12] = T_ln
    cell_L[13] = A_hln
    var_n[pos][-3] = cell_L

    # Extracting the array for the right cell
    cellR = var_n[pos + 1][2]
    x_h1n1r = x_R1                  # Setting the position of the left wall of the right cell
    var_n[pos + 1][1][1] = x_h1n1r  # Over writing the value for the position of the left wall of the right cell (ie the right wall of the cell before it)

    x_h1r = cellR[1]    # Position of the right wall of the right cell
    E_r = cellR[3]      # Specific energy of the right cell
    u_r = cellR[2]      # Velocity of the right cell
    m_r = cellR[7]      # Mass of the right cell

    # Extracting the constants for the right cell
    gamma_r = slugsConst[pos][1]
    Cv_r = slugsConst[pos][0]

    x_rn, m_rn, A_hrn, A_hnrn, rho_rn, e_rn, P_rn, T_rn, a_rn = equationsOfState(x_h1r, x_h1n1r, m_r, systemDiameters, E_r, u_r, gamma_r, Cv_r)

    # Overwritting the values of the right cell in the var_n data structure
    var_n[pos + 1][1][13] = A_hnrn
    cellR[0] = x_rn
    cellR[1] = x_h1r
    cellR[8] = P_rn
    cellR[9] = e_rn
    cellR[10] = a_rn
    cellR[11] = rho_rn
    cellR[12] = T_rn
    cellR[13] = A_hrn
    var_n[pos + 1][2] = cellR

    return var_n


# *********************************************************************************************************************
# ********************************************* Data processing functions *********************************************
# *********************************************************************************************************************

# 1. Create file header
def createHeader(file_name, t_run, dt_out, cfl, order, viscousEffects, heatTransfer, losses, Tw, epsilon, slugs,
                 boundaries, diameters, lCentres, lCoeffs, lLengths, xBuffer, diaphragms, piston_variables,
                 piston_constants, outflow_positions):
    file_header = file_name[:-4] + """

        t_run = """ + str(t_run) + """ s
        dt_out = """ + str(dt_out) + """ s
        cfl = """ + str(cfl) + """
        order = """ + str(order) + """
        viscousEffects = """ + str(viscousEffects) + """
        heatTransfer = """ + str(heatTransfer) + """
        losses = """ + str(losses) + """

        Tw = """ + str(Tw) + """  # wall temperature in K
        epsilon = """ + str(epsilon) + """  # wall roughness

        #[number of cells, start of slug position, end of slug position, pressure, temperature, C_v, gamma, R]
        """

    slug_str = []
    for k in range(len(slugs)):
        file_header = file_header + """slug_""" + str(k) + """ = """ + str(slugs[k]) + """
        """
        slug_str.append("slug_" + str(k))

    file_header = file_header + """slugs = """ + str(slug_str) + """

        boundaries = """ + str(boundaries) + """
        systemDiameters = """ + str(diameters) + """
        lCentres = """ + str(lCentres) + """
        lCoeffs = """ + str(lCoeffs) + """
        lLengths = """ + str(lLengths) + """
        xBuffer = """ + str(xBuffer) + """

        diaphragms = """ + str(diaphragms) + """         #[["intact", burst pressure], [diaphragm 2],[""]]
        pistonVar = """ + str([piston_variables[0]]) + """        #[[time, [Left wall position, velocity, acceleration], [piston 2]...]]
        pistonConst = """ + str(piston_constants) + """     #[[length, mass, Area of seal, coefficient of friction], [piston 2] ...]
        outflow_positions = """ + str(outflow_positions) + """

        """
    return file_header


# 2. Remove ghost cells
def remove_ghost_cells(slugsVar):
    # Loop through all time steps
    for i in range(len(slugsVar)):
        # Loop through all slugs
        for j in range(1, len(slugsVar[i])):
            slugsVar[i][j] = np.delete(slugsVar[i][j], [0, 1], 0)   # Delete the first to cells in the slug
            slugsVar[i][j] = np.delete(slugsVar[i][j], -1, 0)       # Delete the last cell in the slug
            slugsVar[i][j] = np.delete(slugsVar[i][j], -1, 0)       # Delete the last cell in the slug
    return slugsVar


# 3. Combine slugs into 'single slug'
def combine_slugs(slugsVar):

    t = [None] * len(slugsVar)         # Array for storing time values
    data = [None] * len(slugsVar)      # Arra for storing processed data

    # Loop through all time steps
    for i in range(len(slugsVar)):
        t[i] = slugsVar[i][0]       # Set the time value
        s_0 = slugsVar[i][1]        # Extract the 0th slug of the time step
        # Loop through remaining slugs
        for j in range(2, len(slugsVar[i])):
            s_0 = np.concatenate((s_0, slugsVar[i][j]))     # Combine each slug with the combination of slugs before it

        data[i] = s_0   # store the combined slugs in the processed data array

    # Loop through the processed data array
    for i in range(len(data)):
        # Delete all values apart from cell positions, velocities, pressures, temperatures and densities
        data[i] = np.delete(data[i], [1, 3, 4, 5, 6, 7, 9, 10, 13], 1)
    return t, data


# 4. Find the maximum time step that was taken during solving
def max_dt_finder(t):
    # Create an array of the difference between each time step and the time step adjacent to it
    dt = np.diff(t)
    # Find the maximum value of the array
    dt_max = max(dt)
    return dt_max


# 5. Filter out data to ensure the desired output timestep
def filter_data(data, times, dt, pistonVar):

    # Create arrays for filtered values
    filteredGas = []
    filteredPistonPositions = []
    filteredPistonVelocities = []
    filteredTimes = []

    nPistons = len(pistonVar[0]) - 1    # Number of pistons
    t_new = 0

    # Loop through all time steps
    for i in range(len(times)):
        # If the current time step stepped past the new time value calculated by the desired time step value
        if t_new <= times[i] or i == len(times) - 1:
            filteredGas.append(data[i])
            filteredTimes.append(times[i])
            temp_pist_pos = []
            tempPistVel = []
            for j in range(nPistons):
                temp_pist_pos.append(pistonVar[i][j + 1][0])
                tempPistVel.append(pistonVar[i][j + 1][1])
            filteredPistonPositions.append(temp_pist_pos)
            filteredPistonVelocities.append(tempPistVel)
            t_new += dt

    return filteredTimes, filteredGas, filteredPistonPositions, filteredPistonVelocities


# 6. Convert arrays into strings to be written to a text file
def convert_diff(data, times, pistonPos, pistonVel):
    new_data = [None] * 7
    time_str = ""
    nPiston = len(pistonPos[0])
    for i in range(5):
        data_str = ""
        for j in range(len(data)):
            new_str = ""
            for k in range(len(data[j])):
                new_str = new_str + str(data[j][k][i]) + ","
            new_str = new_str + "\n"
            data_str = data_str + new_str
        new_data[i] = data_str

    new_str = ""
    for i in range(len(pistonPos)):
        for j in range(nPiston):
            new_str = new_str + str(pistonPos[i][j]) + ","
        new_str = new_str + "\n"
    new_data[5] = new_str
    new_str = ""
    for i in range(len(pistonVel)):
        for j in range(nPiston):
            new_str = new_str + str(pistonVel[i][j]) + ","
        new_str = new_str + "\n"
    new_data[6] = new_str

    for i in times:
        time_str = time_str + str(i) + ","
    time_str = time_str + "\n"
    return new_data, time_str


# 7. Write data to text file
def writeResults(file_name, file_header, solve_time, time_str, txt_data):
    print("Writing results...")
    write = open(file_name, "w")
    write.writelines(file_header)
    write.writelines("Solving time = " + str(solve_time) + "s\n\n")
    write.writelines("START TIME\n")
    write.writelines(time_str)
    write.writelines("END TIME\n")
    titles = ["POSITION", "VELOCITY", "PRESSURE", "DENSITY", "TEMPERATURE", "PISTON POSITIONS", "PISTON VELOCITIES"]
    for i in range(7):
        write.writelines("START " + titles[i]+"\n")
        write.writelines(txt_data[i])
        write.writelines("END " + titles[i]+"\n")
    write.close()

# *********************************************************************************************************************
# **************************************** Geometric and positional functions *****************************************
# *********************************************************************************************************************

# 1. Get diameter at a position
def diameter(x, system_diameters):
    """"
    Function to get the diameter of the system at a certain position using linear interpolation
    """
    positions = system_diameters[0]  # List of defined diameter positions
    diameters = system_diameters[1]  # List of defined diameters
    for i in range(len(positions) - 1):
        if positions[i + 1] >= x >= positions[i]:
            x_L = positions[i]  # Defined x position on the left of the input position
            x_R = positions[i + 1]  # Defined x position on the right of the input position
            d_L = diameters[i]  # Defined diameter on the left of the input position
            d_R = diameters[i + 1]  # Defined diameter on the right of the input position
            break
    """
    Can't remember why I made the try and except??
    """
    try:
        d = d_L + (x - x_L) * (d_R - d_L) / (x_R - x_L)
    except:
        # print("error\n x=" ,x,"\n xmin=",positions[0],"xmax=", positions[-1])
        d = 0
    return d

# 2. Get area at a position
def area(x, system_diameters):
    """"
    Function to get the area of the system at a certain position using linear interpolation
    """
    d = diameter(x, system_diameters)  # Finding the diameter at the x position
    a = math.pi * (d / 2) ** 2  # Calculating the area (m^2)
    return a


# 3. Find the index of slug on the right of a piston
def pistonSlugPosition(boundaries, pistonNumber):
    n = 0

    for i in range(len(boundaries)):
        if boundaries[i] == "piston" or boundaries[i] == "diaphragm-piston":
            n += 1
        if n == pistonNumber:
            h = i
            n += 1
    return h


# 4. Get volume of a cell
def volume(x_L, x_R, system_diameters):
    """
    Calculating the volume of a cell based on the position of its walls
    """
    """
    This doesn't work. Not sure why.
    """
    positions = system_diameters[0]
    diameters = system_diameters[1]
    cell_positions = [x_L]
    cell_diameters = [diameter(x_L, system_diameters)]
    for i in range(len(positions)):
        if x_R > positions[i] > x_L:
            cell_positions.append(positions[i])
            cell_diameters.append(diameters[i])
    cell_positions.append(x_R)
    cell_diameters.append(diameter(x_R, system_diameters))
    volume = 0
    for i in range(len(cell_positions) - 1):
        dR, dL, xR, xL = cell_diameters[i + 1], cell_diameters[i], cell_positions[i + 1], cell_positions[i]
        m = (dR - dL) / (xR - xL)
        x_int = xL - dL / m
        v1 = math.fabs(x_int - xR) * (1 / 3) * math.pi * (dR / 2) ** 2
        v2 = math.fabs(x_int - xR) * (1 / 3) * math.pi * (dR / 2) ** 2
        volume += math.fabs(v1 - v2)
    return volume

