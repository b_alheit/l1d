"""
Coded by: Benjamin Alheit
"""
import math
import L1d_solver



def main():
    '''
    Define the total running time (t_run), the time step that the solver will output (dt_out) and the cfl number below
    '''
    file_name = "buffer test" + ".txt"
    t_run = 0.01
    dt_out = 0.5e-5
    cfl = 0.3
    order = 2
    viscousEffects = False
    heatTransfer = False
    losses = False
    alarm = False

    Tw = 273 + 20  # wall temperature in K
    epsilon = 5e-6  # wall roughness

    '''
    Define each gas slug and assemble them into a list of slugs. The array for each slug must take the form of:
    slug_a = [number of cells, start of slug position, end of slug position, pressure, temperature, C_v, gamma, R]

    Example:
    slug_a = [40, 0, 0.4, 100*10**5, 287.7, 717.5, 1.4, 286.69]
    slug_b = [40, 0.42, 1, 100*10**3, 287.7, 717.5, 1.4, 286.69]
    slug_c = [40, 1.02, 1.5, 100*10**3, 287.7, 717.5, 1.4, 286.69]

    slugs =[slug_a, slug_b, slug_c]
    '''

    # [number of cells, start of slug position, end of slug position, pressure, temperature, C_v, gamma, R, Pr, S1, T0, mu0]
    slug_a = [25, 0, 0.5, 1e6, 287.7, 717.5, 1.4, 286.69, 0.72, 273.1, 110.4, 16.77e-6]
    slug_b = [25, 0.505, 1, 1e6, 287.7, 717.5, 1.4, 286.69, 0.72, 273.1, 110.4, 16.77e-6]
    slugs = [slug_a, slug_b]

    systemDiameters = [[-1, 2], [0.100, 0.100]]
    xBuffer = [0.7]
    lCentres = []
    lCoeffs = []
    lLengths = []

    '''
    Define the boundary conditions of the system in the order that they occur. Allowable boundary conditions:
        *"wall"
        *"gas-gas"
        *"diaphragm"
        *"piston"
        *"diaphragm-piston"
        *"outflow"

    Example:
        boundaries = ["wall", "piston", "diaphragm-piston", "outflow"]

    If the spelling is incorrect the code will not run.
    '''

    boundaries = ["wall", "piston", "wall"]

    '''
    Define the diaphragm properties, the initial piston variables, the piston constants and the outflow positions.
    These take the form of:
        diaphragms = [["intact", burst pressure], ["burst", burst pressure],[""]] #it is necessary for '[""]' to be the last entry 
    in the array for the code to run.
        pistons_variables =[[time, [Left wall position, velocity, acceleration], [piston 2]...]]
        piston_constants = [[length, mass, Area of seal, coefficient of friction, diameter], [piston 2] ...]
        outflow_positions = [right outflow position, left outflow position]

    Example:
        diaphragms = [["intact",500*10**3],[""]]
        piston_variables = [[0, [0.2, 0, 0], [0.5, 0, 0]]]
        piston_constants = [[0.02, 0.001, math.pi * 0.01 * 0.004, 0.2, 0.1], [0.02, 0.001, math.pi * 0.01 * 0.004, 0.2, 0.1]]
        outflow_positions = [None, 1.5]
    '''
    diaphragms = [[""]]
    pistonVar = [[0, [0.5, 100, 0]]]
    pistonConst = [[0.005, 5, math.pi * 0.01 * 0.004, 0.2, 0.1]]
    outflow_positions = [None, None]

    # DO NOT EDIT FROM HERE ONWARDS
    L1d_solver.solver(file_name, slugs, boundaries, pistonVar, pistonConst, diaphragms, outflow_positions,
                      t_run, dt_out, cfl, order, systemDiameters, epsilon, Tw, viscousEffects, heatTransfer, losses, lCentres,
                      lCoeffs, lLengths, xBuffer, alarm)




main()
