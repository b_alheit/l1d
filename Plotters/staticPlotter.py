"""
Coded by: Benjamin Alheit
I apologise, this code is incredibly messy. Please contact me if you are having trouble editing it
"""
import matplotlib.pyplot as plt
import numpy as np


NCurves = 13
file_name = "DSTO4.txt"

def select_data(time_step_start, time_step_end, steps, total_data):
    index_list = np.arange(time_step_start,time_step_end,steps).tolist()
    selected_data = [None] * len(index_list)
    iterations = 0
    for i in index_list:
        selected_data[iterations] = total_data[i]
        iterations += 1
    return selected_data

def process_data(time,x,u,P,rho,T,time_step_start,time_step_end,steps):
    data = [time, x, u, P, rho, T]
    new_data = [None]*6
    for i in range(6):
        new_data[i] = select_data(time_step_start, time_step_end, steps, data[i])
    print("processed")
    return new_data[0], new_data[1], new_data[2], new_data[3], new_data[4], new_data[5]

def plot_data(time,x,u,P,rho,T):
    figures = [None]*4
    labels = ["Velocity", "Pressure", "Density", "Temperature"]
    units = ["(m/s)", "(Pa)", "(kg/m^3)", "(K)"]
    data = [u,P,rho,T]
    for i in range(len(data)):
        a = plt.figure(i)
        a_plots = []
        for j in range(len(data[i])):
            a, = plt.plot(x[j],data[i][j], label = "t =" + str(time[j]) + " s", marker = "s")
            a_plots.append(a)
        plt.xlabel("X (m)")
        plt.ylabel(labels[i] + units[i])
        plt.title(labels[i] + "Plot")
        plt.legend(handles = a_plots)
        figures[i] = a
    plt.show(figures)


def main():
    data = open(file_name, "r")
    data = data.read()
    time = data[data.find("START TIME"):data.find("END TIME")].split(",")[1:-1]

    for i in range(len(time)):
        time[i] = float(time[i])

    pro_data = [None] * 5
    titles = ["POSITION", "VELOCITY", "PRESSURE", "DENSITY", "TEMPERATURE"]
    for i in range(5):
        temp_data = data[data.find("START "+titles[i]):data.find("END "+ titles[i])].split("\n")[1:-1]
        for j in range(len(temp_data)):
            nlist = temp_data[j].split(",")[:-1]
            for k in range(len(nlist)):
                nlist[k] = float(nlist[k])
            temp_data[j] = nlist
        pro_data[i] = temp_data

    x, u, P, rho, T = pro_data[0], pro_data[1], pro_data[2], pro_data[3], pro_data[4]

    time, x, u, P, rho, T = process_data(time,x,u,P,rho,T,0,len(time) - 1,round((len(time) - 1)/(NCurves)))

    plot_data(time, x, u, P, rho, T)

main()
