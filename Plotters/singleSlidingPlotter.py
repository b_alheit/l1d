"""
Coded by: Benjamin Alheit
Plotting options:
                Pressure: 'P'
                Velocity: 'u'
                Temperature: 'T'
                Density: 'rho'

I apologise, this code is incredibly messy. Please contact me if you are having trouble editing it
"""
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button
import tkinter as tk
from tkinter import filedialog

plot = 'u'

root = tk.Tk()
root.withdraw()

# file_name = filedialog.askopenfilename()
file_name = "../Shock tube/ST3200second.txt"

data = open(file_name, "r")
data = data.read()
time = data[data.find("START TIME"):data.find("END TIME")].split(",")[1:-1]


for i in range(len(time)):
    time[i] = float(time[i])

pro_data = [None] * 6
titles = ["POSITION", "VELOCITY", "PRESSURE", "DENSITY", "TEMPERATURE", "PISTON"]
for i in range(6):
    temp_data = data[data.find("START " + titles[i]):data.find("END " + titles[i])].split("\n")[1:-1]
    for j in range(len(temp_data)):
        nlist = temp_data[j].split(",")[:-1]
        for k in range(len(nlist)):
            nlist[k] = float(nlist[k])
        temp_data[j] = nlist
    pro_data[i] = temp_data

x, u, P, rho, T, xPiston = pro_data[0], pro_data[1], pro_data[2], pro_data[3], pro_data[4], pro_data[5]

def list_list_max(input_list):
    total_max = -9 * 9999
    for i in range(len(input_list)):
        temp_max = max(input_list[i])
        if temp_max > total_max:
            total_max = temp_max
    return total_max


def list_list_min(input_list):
    total_min = 9 ** 9999
    for i in range(len(input_list)):
        temp_min = min(input_list[i])
        if temp_min < total_min:
            total_min = temp_min
    return total_min

nPistons = len(xPiston[0])
fig, ax = plt.subplots()
plt.subplots_adjust(left=0.25, bottom=0.25)


piston_plots = [None] * nPistons
colours = ['black', 'grey', 'yellow', 'red']
for i in range(nPistons):
    p = plt.axvline(xPiston[0][i], label="Piston" + str(1 + i), color=colours[i])
    piston_plots[i] = p

if plot == 'P':
    plot = P
    plt.ylabel("Pressure $(Pa)$")
    handel = "Pressure"
elif plot == 'u':
    plot = u
    plt.ylabel("Velocity $(m/s)$")
    handel = "Velocity"
elif plot == 'T':
    plot = T
    plt.ylabel("Temperature $(K)$")
    handel = "Temperature"
elif plot == 'rho':
    plot = rho
    plt.ylabel("Density  $(kg/m^3)$")
    handel = "Density"

plot_start = plot[0]
x_start = x[0]

l, = plt.plot(x_start, plot_start, linewidth = 1, linestyle = 'solid' , marker='s', color='red', label = handel)
plt.legend(piston_plots)

plt.axis([list_list_min(x), list_list_max(x), list_list_min(plot), list_list_max(plot)])
plt.xlabel("x (m)")
plt.grid()
plt.title(file_name[:-4])
axcolor = 'lightgoldenrodyellow'
axtime = plt.axes([0.25, 0.1, 0.65, 0.03], facecolor=axcolor)

stime = Slider(axtime, 'Time (ms)', 0, max(time) * 1000, valinit=0, valfmt='%1.4f')

def get_index(time, value):
    index = 0
    value = value / 1000
    for i in range(len(time) - 1):
        if time[i + 1] > value >= time[i]:
            index = i
        elif time[i + 1] == value:
            index = i + 1
    return index

def update(val):
    time_index = get_index(time, stime.val)
    y_plt = plot[time_index]
    x_plt = x[time_index]
    l.set_ydata(plot[time_index])
    l.set_xdata(x[time_index])
    for i in range(len(piston_plots)):
        piston_plots[i].set_xdata(xPiston[time_index][i])
    fig.canvas.draw_idle()


stime.on_changed(update)

resetax = plt.axes([0.8, 0.025, 0.1, 0.04])
button = Button(resetax, 'Reset', color=axcolor, hovercolor='0.975')


def reset(event):
    stime.reset()


button.on_clicked(reset)


plt.draw()
plt.grid()
plt.show()

