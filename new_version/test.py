import numpy as np
# import os
#
# # os.mkdir("test_dir")
# # a = np.arange(0, 8).reshape(2,4)
# # b = a[0, :]
# # b[2] = 12
# # print(a)
#
# # a = np.arange(0, 8)
# # a = a.reshape(2, 4)*1.0
# # f = open("./test_dir/test.bin", "a")
# # a[:, 1:3].tofile(f)
# # print(a)
# # b = np.fromfile("./test_dir/test.bin")
# # print(b)
#
# # class test:
# #     def __init__(self):
# #         self.a = np.arange(0, 8).reshape(2,4)
# #         self.b = self.a[1, :]
# #
# #
# #     def print(self):
# #         print('a')
# #         print(self.a)
# #         print('b')
# #         print(self.b)
# #
# #     def change_and_print(self):
# #         # self.b[:] = 1
# #         self.a[1, :] = 1
# #         self.print()
# #
#
#
# #
# my_test = test()
# my_test.print()
# print()
# my_test.change_and_print()
#
# #
# # def check_prime(num, mercine=False):
# #     if num == 1 or num ==2 or num ==3:
# #         return True
# #     for i in range(2, int(num**0.5) + 1):
# #         if num%i == 0:
# #             return False
# #     return True
# #
# # no = 1
# # count = 0
# # N = 227
# # while count < N:
# #     if check_prime(no):
# #         print(no)
# #         count+=1
# #     no +=1
#


def print_a_number(num, actually=True, and_this_number=None):
    if actually:
        print(num)
    if and_this_number is not None:
        print(and_this_number)

# print_a_number(5, and_this_number=1)

print(np.alen(np.array([0,1.00606376523e-05,2.00529938151e-05,3.00205296867e-05,4.00901288446e-05,5.0067572403e-05,6.00476651068e-05,7.00248882101e-05,8.00037279419e-05,9.00852345294e-05,0.00010006399913,0.000110043252374,0.000120022585249,0.000130001516135,0.000140082678748,0.000150061742181,0.000160040936566,0.000170020302232,0.00018010107188,0.000190080333767,0.000200059383738,0.000210038444728,0.000220017799063,0.000230098514709,0.000240077628288,0.000250056732545,0.000260035627969,0.000270014858395,0.000280095579488,0.000290074650861,0.000300053773156,0.000310032563396,0.000320011790588,0.000330092462657,0.000340071485639,0.000350050614387,0.000360029436449,0.00037000858468,0.000380089194599,0.000390068222392,0.000400047299358,0.000410026077012,0.000420005232656,0.000430085922331,0.000440064781856,0.000450043870737,0.00046002266444,0.000470001748995,0.000480082545629,0.000490061344851,0.000500040418715,0.000510019110504,0.000520100041177,0.000530079069413,0.000540057841599,0.000550036934845,0.000560015600573,0.00057009643491,0.00058007540777,0.000590054240116,0.0006])))