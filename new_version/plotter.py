"""
Coded by: Benjamin Alheit
Plotting options:
                Pressure: 'P'
                Velocity: 'u'
                Temperature: 'T'
                Density: 'rho'

I apologise, this code is incredibly messy. Please contact me if you are having trouble editing it
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button
from mpl_toolkits.axes_grid1 import host_subplot
import mpl_toolkits.axisartist as AA
from new_version.L1D_classes import L1D_simulation_results_reader as reader
import tkinter as tk
from tkinter import filedialog

# root = tk.Tk()
# root.withdraw()

# file_name = filedialog.askopenfilename()
# file_name = "../Shock tube/ST3200second.txt"
directory = "test_sim"
results = reader(directory)

"""
data = open(file_name, "r")
data = data.read()
time = data[data.find("START TIME"):data.find("END TIME")].split(",")[1:-1]

for i in range(len(time)):
    time[i] = float(time[i])

pro_data = [None] * 6
titles = ["POSITION", "VELOCITY", "PRESSURE", "DENSITY", "TEMPERATURE", "PISTON"]
for i in range(6):
    temp_data = data[data.find("START " + titles[i]):data.find("END " + titles[i])].split("\n")[1:-1]
    for j in range(len(temp_data)):
        nlist = temp_data[j].split(",")[:-1]
        for k in range(len(nlist)):
            nlist[k] = float(nlist[k])
        temp_data[j] = nlist
    pro_data[i] = temp_data

x, u, P, rho, T, xPiston = pro_data[0], pro_data[1], pro_data[2], pro_data[3], pro_data[4], pro_data[5]

def list_list_max(input_list):
    total_max = -9 * 9999
    for i in range(len(input_list)):
        temp_max = max(input_list[i])
        if temp_max > total_max:
            total_max = temp_max
    return total_max


def list_list_min(input_list):
    total_min = 9 ** 9999
    for i in range(len(input_list)):
        temp_min = min(input_list[i])
        if temp_min < total_min:
            total_min = temp_min
    return total_min
"""

# nPistons = len(xPiston[0])
nPistons = 0
fig, ax = plt.subplots()
plt.subplots_adjust(left=0.08, bottom=0.25, right=0.8)
pres = host_subplot(111, axes_class=AA.Axes)
plt.title(directory)
temp = pres.twinx()
vel = pres.twinx()
den = pres.twinx()

offset_vel = 50
new_fixed_axis = vel.get_grid_helper().new_fixed_axis
vel.axis["right"] = new_fixed_axis(loc="right", axes=vel, offset=(offset_vel, 0))
vel.axis["right"].toggle(all=True)

offset_den = 100
density_axis = den.get_grid_helper().new_fixed_axis
den.axis["right"] = density_axis(loc="right", axes=den, offset=(offset_den, 0))
den.axis["right"].toggle(all=True)

pres.set_xlim(np.min(results.x), np.max(results.x))

pres.set_xlabel("x (m)")
pres.set_ylabel("Pressure (Pa)")
temp.set_ylabel("Temperature (K)")
vel.set_ylabel("Velocity (m/s)")
den.set_ylabel("Density (kg/m^3)")

p1, = pres.plot(results.x[0, :], results.p[0, :], label="Pressure", marker='s')
p2, = temp.plot(results.x[0, :], results.T[0, :], label="Temperature", marker='v')
p3, = vel.plot(results.x[0, :], results.u[0, :], label="Velocity", marker='o')
p4, = den.plot(results.x[0, :], results.rho[0, :], label="Density", marker='D')

piston_plots = [None] * nPistons
colours = ['black', 'grey', 'yellow', 'red']
# for i in range(nPistons):
#     p = plt.axvline(xPiston[0][i], label="Piston" + str(1 + i), color=colours[i])
#     piston_plots[i] = p

pres.set_ylim(np.min(results.p), np.max(results.p))
temp.set_ylim(np.min(results.T), np.max(results.T))
vel.set_ylim(np.min(results.u), np.max(results.u))
den.set_ylim(np.min(results.rho), np.max(results.rho))

pres.legend()

pres.axis["left"].label.set_color(p1.get_color())
temp.axis["right"].label.set_color(p2.get_color())
vel.axis["right"].label.set_color(p3.get_color())
den.axis["right"].label.set_color(p4.get_color())

axcolor = 'lightgoldenrodyellow'
axtime = plt.axes([0.25, 0.1, 0.65, 0.03], facecolor=axcolor)

stime = Slider(axtime, 'Time (ms)', 0, np.max(results.times) * 1000, valinit=0, valfmt='%1.4f')


# def get_index(time, value):
#     index = 0
#     value = value / 1000
#     for i in range(len(time) - 1):
#         if time[i + 1] > value >= time[i]:
#             index = i
#         elif time[i + 1] == value:
#             index = i + 1
#     return index


def update(val):

    time_index = results.get_nearest_index(val/1000)
    p1.set_ydata(results.p[time_index, :])
    p1.set_xdata(results.x[time_index, :])
    p2.set_ydata(results.T[time_index, :])
    p2.set_xdata(results.x[time_index, :])
    p3.set_ydata(results.u[time_index, :])
    p3.set_xdata(results.x[time_index, :])
    p4.set_ydata(results.rho[time_index, :])
    p4.set_xdata(results.x[time_index, :])
    # for i in range(len(piston_plots)):
    #     piston_plots[i].set_xdata(xPiston[time_index][i])
    fig.canvas.draw_idle()


stime.on_changed(update)

resetax = plt.axes([0.8, 0.025, 0.1, 0.04])
button = Button(resetax, 'Reset', color=axcolor, hovercolor='0.975')


def reset(event):
    stime.reset()


button.on_clicked(reset)



plt.draw()
plt.grid()
plt.show()




