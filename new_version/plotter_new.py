"""
Coded by: Benjamin Alheit
Plotting options:
                Pressure: 'P'
                Velocity: 'u'
                Temperature: 'T'
                Density: 'rho'

I apologise, this code is incredibly messy. Please contact me if you are having trouble editing it
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
from matplotlib.widgets import Slider, Button
from mpl_toolkits.axes_grid1 import host_subplot
import mpl_toolkits.axisartist as AA
from new_version.L1D_classes import L1D_simulation_results_reader as reader

directory = "test_sim"
results = reader(directory)
print(results.n_time_steps)


# nPistons = len(xPiston[0])
nPistons = 0
# fig, ax = plt.subplots(3, 1)
# fig, ax = plt.subplots(2, 1, sharey='row')
fig = plt.figure(figsize=(15,10))
gs = gridspec.GridSpec(4, 1, height_ratios=[1, 1, 1, 1])
pres = plt.subplot(gs[0])
temp = plt.subplot(gs[1], sharex=pres)
vel = plt.subplot(gs[2], sharex=pres)
den = plt.subplot(gs[3], sharex=pres)

plt.tight_layout()
plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95, hspace=.0)

axtime = fig.add_axes([0.1, 0.005, 0.8, 0.02])
# pres.get_shared_x_axes().join(pres, temp)
# pres.set_xticklabels([])
plt.setp(pres.get_xticklabels(), visible=False)
plt.setp(temp.get_xticklabels(), visible=False)
plt.setp(vel.get_xticklabels(), visible=False)
# fig, ax = plt.subplots(3, 1, sharex='col', sharey='row')
# plt.subplots_adjust(left=0.08, bottom=0.25, right=0.8)
# pres = host_subplot(111, axes_class=AA.Axes)
pres.set_title(directory)

# pres = ax[0]
# temp = ax[1]
# pres = fig.add_axes([0.1, 0.5, 0.8, 0.4],
#                     xticklabels=[], ylim=(np.min(results.p), np.max(results.p)))
# temp = fig.add_axes([0.1, 0.1, 0.8, 0.4],
#                     xticklabels=[], ylim=(np.min(results.T), np.max(results.T)))
# vel = fig.add_axes()
# den = fig.add_axes()

# offset_vel = 50
# new_fixed_axis = vel.get_grid_helper().new_fixed_axis
# vel.axis["right"] = new_fixed_axis(loc="right", axes=vel, offset=(offset_vel, 0))
# vel.axis["right"].toggle(all=True)
#
# offset_den = 100
# density_axis = den.get_grid_helper().new_fixed_axis
# den.axis["right"] = density_axis(loc="right", axes=den, offset=(offset_den, 0))
# den.axis["right"].toggle(all=True)


pres.set_ylabel("Pressure (Pa)")
temp.set_ylabel("Temperature (K)")
den.set_xlim(np.min(results.x), np.max(results.x))
den.set_xlabel("x (m)")
vel.set_ylabel("Velocity (m/s)")
den.set_ylabel("Density (kg/m^3)")

# p1, = pres.plot(results.x[0, :], results.p[0, :], label="Pressure", marker='s')
# p2, = temp.plot(results.x[0, :], results.T[0, :], label="Temperature", marker='v')
# p3, = vel.plot(results.x[0, :], results.u[0, :], label="Velocity", marker='o')
# p4, = den.plot(results.x[0, :], results.rho[0, :], label="Density", marker='D')


p1, = pres.plot(results.x[0, :], results.p[0, :], label="Pressure")
p2, = temp.plot(results.x[0, :], results.T[0, :], label="Temperature")
p3, = vel.plot(results.x[0, :], results.u[0, :], label="Velocity")
p4, = den.plot(results.x[0, :], results.rho[0, :], label="Density")


piston_plots = [None] * nPistons
colours = ['black', 'grey', 'yellow', 'red']
# for i in range(nPistons):
#     p = plt.axvline(xPiston[0][i], label="Piston" + str(1 + i), color=colours[i])
#     piston_plots[i] = p

pres.set_ylim(0.9*np.min(results.p), 1.1*np.max(results.p))
temp.set_ylim(0.9*np.min(results.T), 1.1*np.max(results.T))
pres.set_xlim(0.9*np.min(results.x), np.max(results.x))
vel.set_ylim(0.9*np.min(results.u), 1.1*np.max(results.u))
den.set_ylim(0.9*np.min(results.rho), 1.1*np.max(results.rho))
pres.grid()
temp.grid()
vel.grid()
den.grid()
# pres.legend()

# pres.axis["left"].label.set_color(p1.get_color())
# temp.axis["right"].label.set_color(p2.get_color())
# vel.axis["right"].label.set_color(p3.get_color())
# den.axis["right"].label.set_color(p4.get_color())

axcolor = 'lightgoldenrodyellow'
# axtime = plt.axes([0.1, 0.05, 0.8, 0.03], facecolor=axcolor)
# axtime = ax[2]
stime = Slider(axtime, 'Time (ms)', 0, np.max(results.times) * 1000, valinit=0, valfmt='%1.4f')


# def get_index(time, value):
#     index = 0
#     value = value / 1000
#     for i in range(len(time) - 1):
#         if time[i + 1] > value >= time[i]:
#             index = i
#         elif time[i + 1] == value:
#             index = i + 1
#     return index


def update(val):

    time_index = results.get_nearest_index(val/1000)
    p1.set_ydata(results.p[time_index, :])
    p1.set_xdata(results.x[time_index, :])
    p2.set_ydata(results.T[time_index, :])
    p2.set_xdata(results.x[time_index, :])
    p3.set_ydata(results.u[time_index, :])
    p3.set_xdata(results.x[time_index, :])
    p4.set_ydata(results.rho[time_index, :])
    p4.set_xdata(results.x[time_index, :])
    # for i in range(len(piston_plots)):
    #     piston_plots[i].set_xdata(xPiston[time_index][i])
    fig.canvas.draw_idle()


stime.on_changed(update)

# resetax = plt.axes([0.8, 0.025, 0.1, 0.04])
# button = Button(resetax, 'Reset', color=axcolor, hovercolor='0.975')
#
#
# def reset(event):
#     stime.reset()
#
#
# button.on_clicked(reset)



plt.draw()
plt.grid()
plt.show()




