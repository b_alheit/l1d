from new_version import L1D_classes as L1D
import numpy as np
import time
# # [number of cells, start of slug position, end of slug position, pressure, temperature, C_v, gamma, R, Pr, T0, S1, mu0]
# slug_0 = [100, 0, 0.5, 1e7, 348.4, 717.5, 1.4, 287.0, 0.72, 273.1, 110.4, 16.77e-6]
# slug_1 = [100, 0.5, 1, 1e4, 278.7, 717.5, 1.4, 287.0, 0.72, 273.1, 110.4, 16.77e-6]

def area(x):
    return np.ones(np.alen(x))


slug_0 = L1D.slug(n_cells_real=1600,
                  x_left=0,
                  x_right=0.5,
                  area=area,
                  p_innit=1.0e7,
                  c_v=717.5,
                  T_innit=348.4,
                  gamma=1.4,
                  R=287.0,
                  Pr=0.72,
                  T0=273.1,
                  S0=110.4,
                  mu0=16.77e6)

slug_1 = L1D.slug(n_cells_real=1600,
                  x_left=0.5,
                  x_right=1,
                  area=area,
                  p_innit=1.0e6,
                  c_v=717.5,
                  T_innit=278.7,
                  gamma=1.4,
                  R=287.0,
                  Pr=0.72,
                  T0=273.1,
                  S0=110.4,
                  mu0=16.77e6)

boundary_conditions = [L1D.wall(slug_0, None),
                       L1D.gas_gas(right_slug=slug_1, left_slug=slug_0),
                       L1D.wall(right_slug=None, left_slug=slug_1)]

slugs = [slug_0, slug_1]
t_end = 0.0006
sim_name = 'test_sim'
t1 = time.time()
my_sim = L1D.L1D_simulation(slugs, boundary_conditions, sim_name, n_recordings=61, t_end=t_end, overwrite=True)
my_sim.run_simulation()
print("time taken: ", time.time()-t1)