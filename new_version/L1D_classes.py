import numpy as np
import os
import shutil
import matplotlib.pyplot as plt

class L1D_simulation_results_reader:
    def __init__(self, sim_directory):
        self.times = np.fromfile("./"+sim_directory+"/time.bin")
        self.n_time_steps = np.alen(self.times)
        self.n_cells_arr = np.fromfile("./"+sim_directory+"/n_cells.bin", dtype=np.int64)
        self.n_cells_tot = np.sum(self.n_cells_arr)
        # self.n_cells = np.fromfile("./"+sim_directory+"/time.bin", dtype=np.int64)
        self.rho = np.fromfile("./"+sim_directory+"/density.bin").reshape(self.n_time_steps, self.n_cells_tot)
        self.p = np.fromfile("./"+sim_directory+"/pressure.bin").reshape(self.n_time_steps, self.n_cells_tot)
        self.T = np.fromfile("./"+sim_directory+"/temperature.bin").reshape(self.n_time_steps, self.n_cells_tot)
        self.u = np.fromfile("./"+sim_directory+"/velocity.bin").reshape(self.n_time_steps, self.n_cells_tot)
        self.x = np.fromfile("./"+sim_directory+"/x.bin").reshape(self.n_time_steps, self.n_cells_tot)

    def get_nearest_index(self, t):
        return np.argmin(np.abs(self.times-t))


class L1D_simulation:
    def __init__(self, slugs, boundary_conditions, simulation_name, n_recordings, overwrite=False, CFL=0.3, t_end=None, piston_exit=None, buffer_hit=None, end_cases=None):
        self.slugs = slugs
        self.boundary_conditions = boundary_conditions
        self.slugs = slugs
        self.cfl = CFL
        self.dt = None
        self.t_end = t_end
        self.t = 0
        self.simulation_name = simulation_name
        self.overwrite = overwrite

        self.dt_write = self.t_end / n_recordings
        self.t_write = self.dt_write

        self.pressure_file = None
        self.velocity_file = None
        self.temperature_file = None
        self.density_file = None
        self.time_file = None
        self.x_file = None

    def run_simulation(self):
        self.init_results_directory()
        self.write_data()

        while self.t < self.t_end:
            self.apply_boundary_conditions()
            # self.plot_shock_tube_pressure()

            self.calc_dt()

            for s in self.slugs:
                s.interpolate()
                s.riemann_solver()
                s.governing_equations()
                s.time_step(self.dt)
                s.equations_of_state()

            self.t += self.dt
            if self.t >= self.t_write:
                print(str(100*self.t/self.t_end)[:4]+"% done")
                self.write_data()
                self.t_write += self.dt_write
                # self.plot_shock_tube_pressure()



        # self.write_data()
        self.close_files()

    def plot_shock_tube_pressure(self):
        for s in self.slugs:
            plt.plot(s.x_centre[2:-2], s.p_centre[2:-2])
            # plt.plot(s.x_centre, s.p_centre)
        plt.show()

    def init_results_directory(self):
        if self.overwrite:
            shutil.rmtree("./"+self.simulation_name, ignore_errors=True)
        os.mkdir(self.simulation_name)
        self.pressure_file = open("./"+self.simulation_name+"/pressure.bin", "a")
        self.velocity_file = open("./"+self.simulation_name+"/velocity.bin", "a")
        self.density_file = open("./"+self.simulation_name+"/density.bin", "a")
        self.temperature_file = open("./"+self.simulation_name+"/temperature.bin", "a")
        self.time_file = open("./"+self.simulation_name+"/time.bin", "a")
        self.x_file = open("./"+self.simulation_name+"/x.bin", "a")
        n_cells = open("./"+self.simulation_name+"/n_cells.bin", "a")
        for s in self.slugs:
            np.array(len(s.p_centre)-4).tofile(n_cells)
        n_cells.close()

    def close_files(self):
        self.pressure_file.close()
        self.velocity_file.close()
        self.density_file.close()
        self.temperature_file.close()
        self.time_file.close()
        self.x_file.close()

    def apply_boundary_conditions(self):
        for bc in self.boundary_conditions:
            bc.assign_ghost_cells()

    def calc_dt(self):
        for i in range(len(self.slugs)):
            if i == 0:
                self.dt = self.slugs[i].del_t()
            else:
                dt = self.slugs[i].del_t()
                if dt < self.dt:
                    self.dt = dt
        self.dt *= self.cfl

    def write_data(self):
        np.array(self.t).tofile(self.time_file)
        for s in self.slugs:
            s.p_centre[2:-2].tofile(self.pressure_file)
            s.u_centre[2:-2].tofile(self.velocity_file)
            s.T_centre[2:-2].tofile(self.temperature_file)
            s.rho_centre[2:-2].tofile(self.density_file)
            s.x_centre[2:-2].tofile(self.x_file)


class slug:
    def __init__(self, n_cells_real, x_left, x_right, area, p_innit, c_v, T_innit, gamma, R, Pr, T0, S0, mu0, u_innit=0, u_dot=0, left_boundary=None, right_boundary=None):
        n_cells = n_cells_real + 4

        self.all_faces = np.zeros([5, n_cells+1])

        self.x_faces = self.all_faces[0, :]
        self.u_faces = self.all_faces[1, :]
        self.A_faces = self.all_faces[2, :]
        self.u_s_faces = self.all_faces[3, :]
        self.p_s_faces = self.all_faces[4, :]

        dx = (x_right - x_left)/n_cells_real

        self.x_faces[:] = np.linspace(x_left - 2 * dx, x_right + 2 * dx, n_cells+1)
        self.u_faces[:] = u_innit
        self.A_faces[:] = area(self.x_faces)

        self.all_centres = np.zeros([11, n_cells])

        self.interpolates_centre = self.all_centres[:4, :]

        self.rho_centre = self.all_centres[0, :]
        self.u_centre = self.all_centres[1, :]
        self.p_centre = self.all_centres[2, :]
        self.a_centre = self.all_centres[3, :]
        self.E_centre = self.all_centres[4, :]
        self.E_dot_centre = self.all_centres[5, :]
        self.e_centre = self.all_centres[6, :]
        self.T_centre = self.all_centres[7, :]
        self.u_dot_centre = self.all_centres[8, :]
        self.x_centre = self.all_centres[9, :]
        self.m_centre = self.all_centres[10, :]

        avg_area = (self.A_faces[0:-1] + self.A_faces[1:]) / 2
        a_init = (gamma * R * T_innit) ** (1 / 2)

        self.rho_centre[:] = (p_innit / (R * T_innit))
        self.u_centre[:] = u_innit
        self.p_centre[:] = p_innit
        self.a_centre[:] = a_init
        self.E_centre[:] = c_v * T_innit
        self.E_dot_centre[:] = 0
        self.e_centre[:] = c_v * T_innit
        self.T_centre[:] = T_innit
        self.u_dot_centre[:] = u_dot
        self.x_centre[:] = np.linspace(x_left - 1.5 * dx, x_right + 1.5 * dx, n_cells)
        self.m_centre[:] = avg_area * dx * self.rho_centre

        self.interpolates_L = np.zeros([4, n_cells-3])
        self.interpolates_R = np.zeros([4, n_cells-3])
        self.interpolates_grad = np.zeros([4, n_cells-3])
        self.interpolates_grad_used = np.zeros([4, n_cells-3])
        self.interpolates_grad_minus = np.zeros([4, n_cells-3])
        self.interpolates_grad_plus = np.zeros([4, n_cells-3])
        self.grad_L = np.zeros([4, n_cells-3])
        self.grad_R = np.zeros([4, n_cells-3])

        self.c_v = c_v
        self.gamma = gamma
        self.R = R
        self.Pr = Pr
        self.T0 = T0
        self.S0 = S0
        self.mu0 = mu0

        self.area = area

    def del_t(self):
        dx = np.abs(self.x_faces[1:] - self.x_faces[0:-1])/2
        return np.min(dx / (np.abs(self.u_centre) + self.a_centre))

    def interpolate(self):
        self.minmod()
        # print(np.alen(self.x_faces[2:-3] - self.x_centre[1:-3]))
        # print(np.alen(self.interpolates_centre[:, 1:-2]))
        # print(np.size(self.grad_L))
        # print(np.size(self.interpolates_L))
        # t = (self.x_faces[2:-2] - self.x_centre[1:-2]) * self.grad_L
        self.interpolates_L = self.interpolates_centre[:, 1:-2] + (self.x_faces[2:-2] - self.x_centre[1:-2]) * self.grad_L
        self.interpolates_R = self.interpolates_centre[:, 2:-1] + (self.x_faces[2:-2] - self.x_centre[2:-1]) * self.grad_R
        # t = 0

    def minmod(self):
        self.interpolates_grad_minus = (self.interpolates_centre[:, 1:-2] - self.interpolates_centre[:, :-3]) / (self.x_centre[1:-2] - self.x_centre[:-3])
        self.interpolates_grad = (self.interpolates_centre[:, 2:-1] - self.interpolates_centre[:, 1:-2]) / (self.x_centre[2:-1] - self.x_centre[1:-2])
        self.interpolates_grad_plus = (self.interpolates_centre[:, 3:] - self.interpolates_centre[:, 2:-1]) / (self.x_centre[3:] - self.x_centre[2:-1])

        diff_sign_L = (np.sign(self.interpolates_grad_minus) != np.sign(self.interpolates_grad)) & (self.interpolates_grad != 0) & (self.interpolates_grad_minus!= 0)
        diff_sign_R = (np.sign(self.interpolates_grad_plus) != np.sign(self.interpolates_grad)) & (self.interpolates_grad != 0) & (self.interpolates_grad_plus != 0)

        far_greater_L = (np.logical_not(diff_sign_L)) & (np.abs(self.interpolates_grad_minus) >= np.abs(self.interpolates_grad))
        far_greater_R = (np.logical_not(diff_sign_R)) & (np.abs(self.interpolates_grad_plus) >= np.abs(self.interpolates_grad))

        # self.grad_L = np.zeros(np.size(self.interpolates_grad))
        # self.grad_R = np.zeros(np.size(self.interpolates_grad))

        # print(self.grad_L[diff_sign_L])

        self.grad_L[far_greater_L] = self.interpolates_grad[far_greater_L]
        self.grad_L[np.logical_not(far_greater_L)] = self.interpolates_grad_minus[np.logical_not(far_greater_L)]
        self.grad_L[diff_sign_L] = 0

        self.grad_R[far_greater_R] = self.interpolates_grad[far_greater_R]
        self.grad_R[np.logical_not(far_greater_R)] = self.interpolates_grad_plus[np.logical_not(far_greater_R)]
        self.grad_R[diff_sign_R] = 0
        t = 0

    def riemann_solver(self):

        rho_L = self.interpolates_L[0,:]
        u_L = self.interpolates_L[1,:]
        P_L = self.interpolates_L[2,:]
        a_L = self.interpolates_L[3,:]

        rho_R = self.interpolates_R[0,:]
        u_R = self.interpolates_R[1,:]
        P_R = self.interpolates_R[2,:]
        a_R = self.interpolates_R[3,:]

        # Calculate  Riemann invariants
        U_L = u_L + 2 * a_L / (self.gamma - 1)
        U_R = u_R - 2 * a_R / (self.gamma - 1)
        Z = (a_R / a_L) * (P_L / P_R) ** ((self.gamma - 1) / (2 * self.gamma))

        # Apply first stage Riemann slover equations
        self.p_s_faces[2:-2] = P_L * (((self.gamma - 1) * (U_L - U_R)) / (2 * a_L * (1 + Z))) ** (2 * self.gamma / (self.gamma - 1))
        self.u_s_faces[2:-2] = (U_L * Z + U_R) / (1 + Z)
        t=0

    def governing_equations(self):
        u_jhn1 = self.u_s_faces[:-1]  # Intermediate velocity of left cell wall
        u_jh = self.u_s_faces[1:]  # Intermediate velocity of right cell wall

        P_jhn1 = self.p_s_faces[:-1]  # Intermediate pressure of right cell wall
        P_jh = self.p_s_faces[1:]  # Intermediate pressure of left cell wall

        A_jh = self.A_faces[1:] # Area of right wall
        A_jhn1 = self.A_faces[:-1]  # Area of left wall

        # Cell scalar values
        m = self.m_centre
        P = self.p_centre

        self.u_faces[:] = self.u_s_faces
        self.u_dot_centre[:] = (P_jhn1 * A_jhn1 - P_jh * A_jh + P * (A_jh - A_jhn1)) / m
        self.E_dot_centre[:] = (P_jhn1 * A_jhn1 * u_jhn1 - P_jh * A_jh * u_jh) / m

    def time_step(self, dt):
        self.x_faces[:] += dt * self.u_faces
        self.u_centre[:] += dt * self.u_dot_centre
        self.E_centre[:] += dt * self.E_dot_centre

    def equations_of_state(self):

        self.x_centre[:] = (self.x_faces[:-1] + self.x_faces[1:]) / 2
        self.A_faces[:] = self.area(self.x_faces)

        A_bar = (self.A_faces[:-1] + self.A_faces[1:])/2
        dx = (self.x_faces[1:] - self.x_faces[:-1])

        self.rho_centre[:] = self.m_centre/(A_bar * dx)
        self.e_centre[:] = self.E_centre - 0.5 * (self.u_centre ** 2)
        self.p_centre[:] = self.rho_centre * (self.gamma-1) * self.e_centre
        self.T_centre[:] = self.e_centre / self.c_v
        self.a_centre[:] = (self.gamma * (self.gamma - 1) * self.e_centre) ** 0.5
        t=0


class boundary_condition:
    def __init__(self, right_slug=None, left_slug=None):
        self.right_slug = right_slug
        self.left_slug = left_slug

    def set_left_slug(self, in_slug):
        self.left_slug = in_slug

    def set_right_slug(self, in_slug):
        self.right_slug = in_slug


class wall:
    def __init__(self, right_slug=None, left_slug=None):
        self.right_slug = right_slug
        self.left_slug = left_slug

    def assign_ghost_cells(self):
        # def swap_values(slug, a, b, c, d, e, f, g, h):
        #     slug.p[a:b] = slug.p[c:d:e]
        #     slug.E[a:b] = slug.E[c:d:e]
        #     slug.e[a:b] = slug.e[c:d:e]
        #     slug.T[a:b] = slug.T[c:d:e]
        #     slug.u_sound[a:b] = slug.u_sound[c:d:e]
        #     slug.rho[a:b] = slug.rho[c:d:e]
        #
        #     slug.u_centre[a:b] = -slug.u_centre[c:d:e]
        #     slug.u_faces[a:b] = -slug.u_faces[c:d:e]
        #     slug.a[a:b] = -slug.a[c:d:e]
        #
        #     wall_position =  slug.x_faces[f]
        #     d_faces = np.abs(wall_position-slug.x_faces[g:h:e])
        #     d_centres = np.abs(wall_position-slug.x_centres[c:d:e])
        #
        #     slug.x_faces[a:b] = wall_position + e * d_faces
        #     slug.x_centres[a:b] = wall_position + e * d_centres

        if self.right_slug is not None:
            a = self.right_slug
            a.all_centres[:, 0] = a.all_centres[:, 3]
            a.all_centres[:, 1] = a.all_centres[:, 2]

            a.u_centre[0] = -a.u_centre[3]
            a.u_centre[1] = -a.u_centre[2]

            a.u_dot_centre[0] = -a.u_dot_centre[3]
            a.u_dot_centre[1] = -a.u_dot_centre[2]

            a.x_centre[0] = a.x_faces[2] - (a.x_centre[3] - a.x_faces[2])
            a.x_centre[1] = a.x_faces[2] - (a.x_centre[2] - a.x_faces[2])

            a.all_faces[:, 0] = a.all_faces[:, 4]
            a.all_faces[:, 1] = a.all_faces[:, 3]

            a.u_s_faces[0] = -a.u_s_faces[4]
            a.u_s_faces[1] = -a.u_s_faces[3]

            a.x_faces[0] = a.x_faces[2] - (a.x_faces[4] - a.x_faces[2])
            a.x_faces[1] = a.x_faces[2] - (a.x_faces[3] - a.x_faces[2])

            # swap_values(self.super.right_slug,
            #             a=0,
            #             b=2,
            #             c=3,
            #             d=1,
            #             e=-1,
            #             f=2,
            #             g=4,
            #             h=2)

        if self.left_slug is not None:
            a = self.left_slug
            a.all_centres[:, -1] = a.all_centres[:, -4]
            a.all_centres[:, -2] = a.all_centres[:, -3]

            a.u_centre[-1] = -a.u_centre[-4]
            a.u_centre[-2] = -a.u_centre[-3]

            a.u_dot_centre[-1] = -a.u_dot_centre[-4]
            a.u_dot_centre[-2] = -a.u_dot_centre[-3]

            a.x_centre[-1] = a.x_faces[-3] + (a.x_faces[-3] - a.x_centre[-4])
            a.x_centre[-2] = a.x_faces[-3] + (a.x_faces[-3] - a.x_centre[-3])

            a.all_faces[:, -1] = a.all_faces[:, -5]
            a.all_faces[:, -2] = a.all_faces[:, -4]

            a.u_s_faces[-1] = -a.u_s_faces[-5]
            a.u_s_faces[-2] = -a.u_s_faces[-4]

            a.x_faces[-1] = a.x_faces[-3] + (a.x_faces[-3] - a.x_faces[-5])
            a.x_faces[-2] = a.x_faces[-3] + (a.x_faces[-3] - a.x_faces[-4])

            # swap_values(self.super.left_slug,
            #             a=-2,
            #             b=None,
            #             c=-3,
            #             d=-5,
            #             e=-1,
            #             f=-3,
            #             g=-4,
            #             h=-6)


class gas_gas:
    def __init__(self, right_slug=None, left_slug=None):
        self.right_slug = right_slug
        self.left_slug = left_slug
    def assign_ghost_cells(self):
        # def swap_values(slug_1, slug_2, a, b, c, d):
        #     slug_1.p[a:b] = slug_2.p[c:d]
        #     slug_1.E[a:b] = slug_2.E[c:d]
        #     slug_1.e[a:b] = slug_2.e[c:d]
        #     slug_1.T[a:b] = slug_2.T[c:d]
        #     slug_1.u_sound[a:b] = slug_2.u_sound[c:d]
        #     slug_1.rho[a:b] = slug_2.rho[c:d]
        #     slug_1.u_centre[a:b] = slug_2.u_centre[c:d]
        #     slug_1.u_faces[a:b] = slug_2.u_faces[c:d]
        #     slug_1.a[a:b] = slug_2.a[c:d]
        #     slug_1.x_faces[a:b] = slug_2.x_faces[c:d]
        #     slug_1.x_centres[a:b] = slug_2.x_centres[c:d]

        right = self.right_slug
        left = self.left_slug
        left.all_centres[:, -2:] = right.all_centres[:, 2:4]
        left.all_faces[:, -3:] = right.all_faces[:, 2:5]

        right.all_centres[:, :2] = left.all_centres[:, -4:-2]
        right.all_faces[:, :3] = left.all_faces[:, -5:-2]

        # swap_values(self.right_slug, self.left_slug, 0, 2, -4, -2)
        # swap_values(self.left_slug, self.right_slug, -2, None, 2, 4)


class diaphragm:
    def __init__(self, p_burst, right_slug=None, left_slug=None):
        self.right_slug = right_slug
        self.left_slug = left_slug
        self.wall_condition = wall(self.right_slug, self.left_slug)
        self.gas_condition = gas_gas(self.right_slug, self.left_slug)

        self.p_burst = p_burst
        self.burst = False

    def check_burst(self):
        right = self.right_slug
        left = self.left_slug
        self.burst = self.p_burst <= np.abs(right.p_centre[2] - left.p_centre[-3])

    def assign_ghost_cells(self):
        if self.p_burst:
            self.gas_condition.assign_ghost_cells()
        else:
            self.check_burst()
            if self.p_burst:
                self.gas_condition.assign_ghost_cells()
            else:
                self.wall_condition.assign_ghost_cells()

# TODO: Create all of the classes outlined bellow
# class piston:
#
#
# class diaphragm_piston:
#
